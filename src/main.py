import os
import sys
from mvc.controller.manager import Manager

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

LOG = './log'
if not os.path.exists(LOG): os.makedirs(LOG)

manger = Manager()

"""Model

Módulo que crea instancias para resolver VRPTW con AG.
"""

from typing import Callable, List, Tuple
from pandas import DataFrame

from mvc.controller.observer import Observable
from mvc.controller.sound_manager import SoundManager
from mvc.model.chromosome import Chromosome
from mvc.model.genetic_algorithm import Algorithm

ALL_LOG = 'log/average-'
RESULT_LOG = 'log/result-'
EXTENSION = '.log'

GEN_STR =\
'[Generación {:d}]\n\
\t[Aptitud Promedio] {:.2f}\n\
\t[Cantidad de individuos] {:d}\n\
\t[Mejor Solución]\n\
\t\t[Aptitud] {:.2f}\n\
\t\t[Distancia] {:.2f}\n\
\t\t[Tiempo] {:.2f}\n\
\t\t[# Rutas] {:d}\n'

RESULT_STR = '[{:s}] {:s}\n\n'
FINAL_STR = '[OBTENIDO EN LA ITERACIÓN] {:d}\n[TIEMPO EN EJECUCIÓN] {:.2f}'

class Model(Observable):
    """
    Clase que sirve a Manager para pedir ejecuciones de la simulación.

    Attributes:
        __first_generation: Si se trata de ejecutar el AG por primera
            vez.
        __ga: La lógica actual del AG.

    Methods:
        set_parameters: Crea la lógica del AG con los parámetros
            indicados.
        do_simulation: Obtiene la siguiente generación del AG para la
            lógica actual.
        stop: Indica que ya se terminó la ejecución del AG y la próxima
            vez que se trate de iniciar, creará una nueva instancia de
            la lógica con los nuevo parámetros.
    """

    __first_generation = True
    sound = SoundManager()

    def get_default(
            self, data: DataFrame,
            max_capacity: int,
            fitness: Callable[
                [float, Tuple[int, int, int], float], float
            ], min_vehicles: int, max_vehicles: int,
            solution: Tuple[int], name: str, break_points: List[int]
        ) -> None:
        """
        Asignan un conjunto de parámetros para obtener la visualización
        de una solución predeterminada
        """

        chromosome = Chromosome(
            data, max_capacity, (min_vehicles, max_vehicles),
            float('inf'), fitness, solution, False, break_points
        )
        filename = name + EXTENSION
        with open(RESULT_LOG + filename, 'w') as log:
            log.write(
                RESULT_STR.format(
                    'Individuo predeterminado', str(chromosome)
                )
            )
        self.notify_observers('default', chromosome)


    def set_parameters(
            self, data: DataFrame,
            max_capacity: int, max_operation_time: float,
            iterations: int, population_size: int, max_non_productive: int,
            mutation_rate: float, selection_method: str,
            crossover: Callable[
                [Tuple[int], Tuple[int]], Tuple[Tuple[int], Tuple[int]]
            ],
            near_distance: int,
            fitness: Callable[
                [float, Tuple[int, int, int], float], float
            ],
            min_vehicles: int, max_vehicles: int,
            name: str, better_son: bool, always_mut: bool
        ) -> None:
        """
        Asignan un conjunto de parámetros para la simulación.

        Args:
            data (DataFrame): Toda la información de los clientes.
            max_capacity (int): Cantidad máxima de productos que puede
                transportar el vehículo.
            max_operation_time (float): Cantidad máxima de tiempo que
                dispone el vehículo para terminar una ruta.
            iterations (int): La cantidad de iteraciones que hará el
                algoritmo.
            population_size (int): Cantidad de individuos por
                generación.
            mutation_rate (float): La taza de mutación en la
                descendencia.
            selection_method (str): Un identificador para el método de
                selección.
            crossover (Callable): Una función para realizar los
                cruzamientos.
            near_distance (int): La distancia que tienen que tener los
                clientes entre sí para ser considerados como cercanos.
            fitness (Callable): Una función para fenerar los valores
                numéricos de la aptitud.
            min_vehicles (int): La cantidad mínima de vehículos que hay
                para las soluciones.
            max_vehicles (int): La cantidad máxima de vehículos que hay
                para las soluciones.
        """

        self.__ga = Algorithm(
            data, max_capacity, max_operation_time,
            iterations, population_size, mutation_rate, max_non_productive,
            near_distance, min_vehicles, max_vehicles
        )
        self.__ga.selection_method = selection_method
        self.__ga.crossover = crossover
        self.__ga.fitness = fitness
        self.__ga.better_son = better_son
        self.__ga.always_mut = always_mut
        self.__first_generation = True
        self.__name = name + EXTENSION

    def do_simulation(
            self, iteration: int, last_productive: int
        ) -> Tuple[int, bool]:
        """
        Obtiene la siguiente generación de individuos, y notifica de
        los datos importantes.

        Args:
            iteration (int): El número de la iteración que se está
                haciendo.
            last_productive (int): La cantidad de iteraciones que han
                pasado desde la última vez que se tuvo una generación
                productiva.

        Returns:
            Un número que sirve de identificador único para el mejor
            resultado del algoritmo.
        """

        if last_productive == 0: self.__iteration = iteration
        if self.__first_generation == True:
            self.__ga.first_generation()
            self.__first_generation = False
            with open(RESULT_LOG + self.__name, 'w') as log:
                log.write(
                    RESULT_STR.format(
                        'Primer Individuo', str(self.__ga.best_individual)
                    )
                )
            file = open(ALL_LOG + self.__name, 'w')
            file.close()
        else: self.__ga.next_generation()
        with open(ALL_LOG + self.__name, 'a') as log:
            info = self.__ga.current_info()
            log.write(GEN_STR.format(
                iteration, info[0], info[1], info[2], info[3], info[4], info[5]
            ))

        self.notify_observers(
            'next_gen', (iteration, last_productive, self.__ga.best_individual)
        )
        return (hash(self.__ga.best_individual), info[1] < 2)

    def stop(self, time: float) -> None:
        """
        Notifica a los observadores que ya no va a entregar más datos
        de la simulación.

        Args:
            time (float): El tiempo en ejecución del algoritmo.
        """

        self.__first_generation = True
        with open(RESULT_LOG + self.__name, 'a') as log:
            log.write(
                RESULT_STR.format(
                    'Mejor Individuo', str(self.__ga.best_individual)
                )
            )
            log.write(FINAL_STR.format(self.__iteration, time))
        if hasattr(self, '__ga'): delattr(self, '__ga')
        self.sound.play_sound()
        self.notify_observers('stop')

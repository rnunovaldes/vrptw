"""Genetic Algorithm

Módulo con una clase que puede usar un algoritmo genético para resolver
VRPTW con distintos métodos de cruzamiento y selección de padres.
"""

import random
from typing import Callable, List, Tuple
from pandas import DataFrame
from concurrent.futures import ThreadPoolExecutor
from threading import RLock

from mvc.model.chromosome import Chromosome
from mvc.model.clusterer import Clusterer
from mvc.model.crossover_methods import one_point_crossover
from mvc.model.fitness_functions import fitness_distance

THREADS = 12

def valid_percentage(percentage: float) -> float:
    """
    Valida que un número sea un porcentaje y si lo es, lo regresa, en
    caso contrario, da el número más cercano de éste entre 0 y 100.

    Args:
        percentage (float): En número a limitar si es que es mayor a
            100 o menor a 0.
    """

    if percentage < 0: return 0
    elif percentage > 100: return 100
    else: return percentage

selection_identifiers = ['tournament', 'elitims', 'roulette']
tournament_size = 2
elite_percentage = 30
elite_children_percentage = 50

class AtomicInt:
    """
    Clase que permite crear un entero que es seguro para usar en
    entornos multihilo.

    Attributes:
      __value: El valor actual del entero.

    Methods:
      increment: Permite incrementar el valor del entero en una cantidad.
      decrement: Permite decrementar el valor del entero en una cantidad.
    """

    def __init__(self, initial: int=0):
        """

        """

        self.__value = initial
        self._lock = RLock()

    def increment(self, value: int=1) -> None:
        """
        Incrementa el valor del entero en una cantidad especificada.

        Args:
          value (int): La cantidad a incrementar en el objeto. No puede
            ser menor a 1 y si no se da valor alguno usa 1 por defecto
        """

        if value < 1: value = 1
        with self._lock:
            self.__value += value

    def decrement(self, value: int=1) -> None:
        """
        Decrementa el valor del entero en una cantidad especificada.

        Args:
          value (int): La cantidad a decrementar en el objeto. No puede
            ser menor a 1 y si no se da valor alguno usa 1 por defecto
        """

        if value < 1: value = 1
        with self._lock:
            self.__value -= value

    @property
    def value(self) -> int:
        """
        Obtiene el valor actual de la varible.

        Returns:
            El valor actual de la variable.
        """

        return self.__value

    @value.setter
    def value(self, new_value: int) -> None:
        """
        Permite asignar un nuevo valor a la varible.
        """

        self.__value = new_value


    def __lt__(self, other):
        if isinstance(other, (AtomicInt, int)):
            return self.__value < (
                    other.value if isinstance(other, AtomicInt) else other
                )
        return NotImplemented

    def __le__(self, other):
        if isinstance(other, (AtomicInt, int)):
            return self.__value <= (
                    other.value if isinstance(other, AtomicInt) else other
                )
        return NotImplemented

    def __eq__(self, other):
        if isinstance(other, (AtomicInt, int)):
            return self.__value == (
                    other.value if isinstance(other, AtomicInt) else other
                )
        return NotImplemented

    def __ne__(self, other):
        if isinstance(other, (AtomicInt, int)):
            return self.__value != (
                    other.value if isinstance(other, AtomicInt) else other
                )
        return NotImplemented

    def __gt__(self, other):
        if isinstance(other, (AtomicInt, int)):
            return self.__value > (
                    other.value if isinstance(other, AtomicInt) else other
                )
        return NotImplemented

    def __ge__(self, other):
        if isinstance(other, (AtomicInt, int)):
            return self.__value >= (
                    other.value if isinstance(other, AtomicInt) else other
                )
        return NotImplemented

    def __add__(self, value):
        if isinstance(value, int):
            self.increment(value)
            return self.__value
        return NotImplemented

    def __radd__(self, other):
        return self.__add__(other)

    def __sub__(self, value):
        if isinstance(value, int):
            self.decrement(value)
            return self.__value
        return NotImplemented

    def __rsub__(self, other):
        return self.__sub__(other)

    def __str__(self):
        return str(self.__value)

class Algorithm():
    """
    Clase que puede resolver VRPTW.

    Attributes:
        __data: Tiene información sobre la ubicación de los clientes,
            cantidad de productos que quieren, ventanas de tiempo en
            que pueden ser despachados y el tiempo que se tarda ello.
        __max_capacity: La capacidad máxima de productos del vehículo
            por viaje.
        __max_operation_time: El tiempo máximo que puede el vehículo
            estar en una ruta.
        __max_iterations: La cantidad de iteraciones que se van a
            hacer.
        __max_non_productive: La máxima cantidad de hijos no
            productivos generados consecutivos.
        __population_size: La cantidad de individuos que habrán por
            generación.
        __mutation_rate: La probabilidad de que un individuo mute.
        __current_population: Una lista con los individuos de la
            generación actual.
        __selection_method: Un identificador del tipo de selección de
            padres que se van a usar.
        __crossover: Una función que toma dos listas y crea una en base
            a ellas.
        __fitness: Una función para poder calcular el valor numérico de
            la aptitud de las soluciones.
        __best_individual: El individuo mejor adaptado de todas las
            generaciones.
        __max_time_without_feasible: La cantidad de iteraciones que
            pueden pasar en una generación sin que se cree un nuevo
            individuo antes de pasar a la siguiente generación.
        _time_constant: Una constante numérica usada para restar tiempo
            a la cantidad de veces que se ha intentado obtener un hijo
            sin éxito.
        __distance: La distancia que es considerada para determinar si
            dos clientes son cercanos entre sí o no.
        __vehicle_info: Una tupla con los datos de la cantidad mínima
            de vehículos y la máxima para las soluciones.

    Methods:
        __generate_child: Genera un hijo en base a dos individuos.
        average_fitness: Obtiene el promedio de los valores de aptitud
            de la generación actual.
        first_generation: Crea una primera generación de manera
            aleatoria.
        __tournament_selection: Selecciona dos indivduos por torneo
            binario.
        __elitism_selection: Selecciona dos individuos por elitismo.
        __roulette_selection: Selecciona los individuos por ruleta.
        next_generation: Obtiene una nueva población en base a la
            generación actual.
        start_genetic_algorithm: Inicia una ejecución del AG.
    """

    def __init__(
            self, data: DataFrame,
            max_capacity: int, max_operation_time: float=float('inf'),
            iterations: int=100, population_size: int=100,
            mutation_rate: float=5, max_time_without_feasible: int=100,
            distance: int=5, min_vehicles: int=1, max_vehicles: int=99
        ):
        """
        Constructor para realizar procedimientos del algoritmo
        genético.

        Parameters:
            data (DataFrame): Toda la información de los clientes.
            max_capacity (int): Cantidad máxima de productos que puede
                transportar el vehículo.
            max_operation_time (float): Cantidad máxima de tiempo que
                dispone el vehículo para terminar una ruta. Si no se
                especifica, es float('inf').
            iterations (int): La cantidad de iteraciones que hará el
                algoritmo. Si no se especifica, su valor es 100.
            population_size (int): Cantidad de individuos por
                generación. Si no se especifica, su valor es 100.
            mutation_rate (float): La taza de mutación en la
                descendencia. Si no se especifica, su valor es 5.
            time_without_feasible_solution (int): La cantidad de
                iteraciones que pueden pasar tratando de generar un
                hijo sin obtener resultados.
            distance (int): La distancia que es considerada para
                determinar si dos clientes son cercanos entre sí o no.
            min_vehicles (int): La cantidad mínima de vehículos que hay
                para las soluciones.
            max_vehicles (int): La cantidad máxima de vehículos que hay
                para las soluciones.
        """

        self.__data = data
        self.__max_capacity = max_capacity
        self.__max_operation_time = max_operation_time
        self.__max_iterations = iterations
        self.__population_size = population_size
        self.__mutation_rate = valid_percentage(mutation_rate)
        self.__current_population: List[Chromosome] = []
        self.__selection_method = 'tournament'
        self.__crossover: Callable[
            [List[int], List[int]], Tuple[List[int], List[int]]
        ] = one_point_crossover
        self.__fitness: Callable[
            [float, Tuple[int, int, int], float], float
        ] = fitness_distance
        self.__vehicle_info = (min_vehicles, max_vehicles)
        self.__best_individual: Chromosome = None
        self.__max_time_without_feasible = max_time_without_feasible
        self._time_constant = 4#math.ceil(max_time_without_feasible / 10)
        self.__distance = distance
        self.__always_mut = False
        self.__better_son = True

    @property
    def selection_method(self) -> str:
        """
        Obtiene el nombre del método de selección de padres usado.

        Returns:
            Una cadena que identifica al método de selección.
        """

        return self.__selection_method

    @selection_method.setter
    def selection_method(
            self, new_selection_method: str
        ) -> None:
        """
        Asigna un nuevo método de selección de padres para ser usado. Si
        no se cambia, usa selección por torneo binario.

        Args:
            new_selection_method (str): Una cadena que identifica al
                método de selección deseado.
        """
        identifier = new_selection_method.lower()
        if identifier in selection_identifiers:
            self.__selection_method = identifier

    @property
    def crossover(self) -> Callable[
            [Tuple[int, ...], Tuple[int, ...]],
            Tuple[Tuple[int, ...], Tuple[int, ...]]
        ]:
        """
        Obtiene la función que está siendo usada para el cruzamiento.

        Returns:
            Una función que recibe dos listas de enteros y devuelve una
            pareja de listas de enteros.
        """

        return self.__crossover

    @crossover.setter
    def crossover(
            self, new_crossover: Callable[
                [Tuple[int, ...], Tuple[int, ...]],
                Tuple[Tuple[int, ...], Tuple[int, ...]]
            ]
        ) -> None:
        """
        Asigna una nueva función de cruzamiento para el algoritmo. Si
        no se cambia, usa el cruzamiento de un puto por defecto.

        Args:
            new_crossover (Calleble): Una función que recibe dos listas
                de enteros y devuelve dos listas de enteros.
        """

        self.__crossover = new_crossover

    @property
    def fitness(self) -> Callable[
            [float, Tuple[int, int, int], float], float
        ]:
        """
        Obtiene la función que está siendo usada para el cálculo de la
        aptitud.

        Returns:
            Una función que recibe dos números flotantes y una tupla de
            tres enteros.
        """

        return self.__fitness

    @crossover.setter
    def fitness(
            self, new_fitness: Callable[
                [float, Tuple[int, int, int], float], float
            ]
        ) -> None:
        """
        Asigna una nueva función de aptitud para el algoritmo. Si
        no se cambia, usa la función que da preferencia a la distancia
        en bruto por defecto.

        Args:
            new_fitness (Calleble): Una función que recibe dos números
            flotantes y una tupla de tres enteros.
        """

        self.__fitness = new_fitness

    @property
    def better_son(self) -> bool:
        """
        """

        return self.__better_son

    @better_son.setter
    def better_son(self, new_better_son: bool) -> None:
        """
        """

        self.__better_son = new_better_son

    @property
    def always_mut(self) -> bool:
        """
        """

        return self.__always_mut

    @always_mut.setter
    def always_mut(self, new_always_mut: bool) -> None:
        """
        """

        self.__always_mut = new_always_mut

    @property
    def best_individual(self) -> Chromosome:
        """
        Obtiene al mejor individuo de todas las generaciones.

        Returns:
            El individuo que mejor se adapto a lo largo de todas las
            generaciones.
        """

        return self.__best_individual

    def current_info(self) -> Tuple[float, int, float, float, float, int]:
        """
        Obtiene información de la generación actual como el promedio de
        la aptitud de esta, la cantidad de indiviuos que tiene y los
        datos del mejor individuo como su aptitud, distancia, tiempo y
        cantidad de rutas.

        Returns:
            Una tupla con 4 números de punto flotante y dos enteros.
        """

        average = sum(
            solution.fitness_calc() for solution in self.__current_population
        ) / len(self.__current_population)
        return (
            average, len(self.__current_population),
            self.__current_population[0].fitness_calc(),
            self.__current_population[0].total_distance,
            self.__current_population[0].total_time,
            len(self.__current_population[0].routes))

    def __generate_child(
            self, parent_1: Chromosome, parent_2: Chromosome
        ) -> Chromosome:
        """
        Genera la descendencia de dos padres dados. Se crean dos hijos
        a partir de ellos y se devuelve al azar un de los dos.

        Args:
            parent_1 (Chromosome): El primer padre.
            parent_2 (Chromosome): El segundo padre.

        Returns:
            El hijo generado escogido del cruzamiento de los padres
        """

        children = self.__crossover(parent_1.solution, parent_2.solution)
        child_1 = Chromosome(
            self.__data, self.__max_capacity,
            self.__vehicle_info, self.__max_operation_time,
            self.__fitness, children[0], self.__always_mut
        )
        child_2 = Chromosome(
            self.__data, self.__max_capacity,
            self.__vehicle_info, self.__max_operation_time,
            self.__fitness, children[1], self.__always_mut
        )
        if self.__better_son:
            return min(
                child_1, child_2, key=lambda solution: solution.fitness_calc()
            )
        else:
            return random.choice((child_1, child_2))

    def first_generation(self) -> None:
        """
        Genera la primera generación de individuos para el AG de forma
        aleatoria, pero asegurándose que sean todas soluciones
        plausibles.
        """

        clusterer = Clusterer(self.__data, self.__distance)
        lock = RLock()

        def generate_solution():
            while True:
                solution = Chromosome(
                    self.__data, self.__max_capacity,
                    self.__vehicle_info, self.__max_operation_time,
                    self.__fitness, clusterer.generate_solution(),
                    self.__always_mut
                )
                if solution.feasible: return solution

        with ThreadPoolExecutor(max_workers=THREADS) as executor:
            while len(self.__current_population) < self.__population_size:
                future = executor.submit(generate_solution)
                solution = future.result()
                with lock:
                    if (solution != None and solution not in self.__current_population
                            and len(self.__current_population) < self.__population_size):
                        self.__current_population.append(solution)

        # Ordena la población por aptitud y selecciona el mejor individuo
        self.__current_population = sorted(
            self.__current_population,
            key=lambda solution: solution.fitness_calc()
        )
        self.__best_individual = self.__current_population[0]

    def __tournament_selection(
            self, tournament_size: int=2
        ) -> List[Chromosome]:
        """
        Selecciona dos individuos de una lista de los posibles padres
        por torneo. El torneo se realiza seleccionando una cantidad
        fija de individuos al azar y seleccionando al mejor adaptado de
        esta selección.

        Args:
            tournament_size (int): La cantidad de individuos a ser
                escogidos por ronda del torneo.

        Returns:
            Una lista que tiene a la descendencia creada con esta
            selección. Es del mismo tamaño que la población que la
            crea.
        """

        if tournament_size < 2: tournament_size = 2

        def get_parents() -> Tuple[Chromosome, Chromosome]:
            """
            Obtiene a dos individuos de la población actual para que
            sean padres para este método de seleción.

            Returns:
                Una tupla de cromosomas en la pobleción a tratar como
                padres.
            """

            first_index = min(random.sample(range(len(self.__current_population)), tournament_size))
            second_index = min(random.sample(range(len(self.__current_population)), tournament_size))

            # Asegura que los padres no sean el mismo individuo
            while first_index == second_index:
                second_index = min(random.sample(range(len(self.__current_population)), tournament_size))

            return (
                self.__current_population[first_index],
                self.__current_population[second_index]
            )

        def generate_feasible_child():
            """
            Genera un hijo factible con padres seleccionados por torneo.
            """

            nonlocal time_without_feasible
            while time_without_feasible < self.__max_time_without_feasible:
                time_without_feasible += 1
                parents = get_parents()
                child = self.__generate_child(parents[0], parents[1])
                if child.feasible:
                    time_without_feasible -= self._time_constant\
                        if time_without_feasible >= self._time_constant else 0
                    return child

        lock = RLock()
        time_without_feasible = AtomicInt()
        new_population: List[Chromosome] = []
        limit = False
        with ThreadPoolExecutor(max_workers=THREADS) as executor:
            while len(new_population) < self.__population_size and not limit:
                limit = time_without_feasible >= self.__max_time_without_feasible

                future = executor.submit(generate_feasible_child)
                solution = future.result()
                with lock:
                    if (solution != None and solution not in new_population
                            and len(new_population) < self.__population_size):
                        new_population.append(solution)

        return new_population

    def __elitism_selection(
            self, elite_percentage: float=0, elite_children_percentage: float=0
        ) -> List[Chromosome]:
        """
        Selecciona al azar a dos individuos élite en el porcentage
        dado. Si es 0 o 100 por ciento, es como escoger al azar en toda
        la población.

        Args:
            elite_percentage (float): El porcentage de individuos que
                serán considerados élite.
            elite_children_percentage (float): El porcentage de hijos
                que serán creados por los individuos élite.

        Returns:
            Una lista que tiene a la descendencia creada con esta
            selección. Es del mismo tamaño que la población que la
            crea.
        """

        elite_size = int(
            (len(self.__current_population) * valid_percentage(elite_percentage)) / 100
        )
        elite_children_size = int(
            (len(self.__current_population) * valid_percentage(elite_children_percentage)) / 100
        )

        def get_parents(limit: int) -> Tuple[Chromosome, Chromosome]:
            """
            Obtiene a dos individuos de la población actual para que
            sean padres para este método de seleción.

            Args:
                limit (int): Hasta que índice de la población se limita
                    la selección de padres para escoger solo un grupo
                    de élite.

            Returns:
                Una tupla de cromosomas en la pobleción a tratar como
                padres.
            """

            first_index = random.randint(0, limit)
            second_index = random.randint(0, limit)
            while first_index == second_index:
                second_index = random.randint(0, limit)
            return (
                self.__current_population[first_index],
                self.__current_population[second_index]
            )

        def generate_feasible_child(current_size: int):
            """
            Genera un hijo factible con padres seleccionados por torneo.
            """

            nonlocal time_without_feasible

            elite = elite_size > 1 and current_size < elite_children_size
            sample_size = elite_size if elite else self.__population_size

            parents = get_parents(sample_size)
            child = self.__generate_child(parents[0], parents[1])
            time_without_feasible += 1
            if child.feasible and child not in new_population:
                time_without_feasible -= self._time_constant\
                    if time_without_feasible >= self._time_constant else 0
                return (child, elite)

        lock = RLock()
        limit: bool = False
        time_without_feasible = AtomicInt()
        new_population: List[Chromosome] = []
        with ThreadPoolExecutor(max_workers=THREADS) as executor:
            while len(new_population) < self.__population_size and not limit:
                limit = time_without_feasible >= self.__max_time_without_feasible
                future = executor.submit(
                    generate_feasible_child, len(new_population)
                )
                solution = future.result()
                with lock:
                    if (solution != None and solution[0] not in new_population
                            and len(new_population) < self.__population_size):
                        if ((len(new_population) < elite_children_size and solution[1])
                                or not solution[1]):
                            new_population.append(solution[0])

        return new_population

    def __roulette_wheel_selection(self) -> List[Chromosome]:
        """
        Selecciona por ruleta a dos individuos. Entre más apto sea el
        individuo, más probable es que sea seleccionado.

        Returns:
            Una lista que tiene a la descendencia creada con esta
            selección. Es del mismo tamaño que la población que la
            crea.
        """

        def get_parents() -> Tuple[Chromosome, Chromosome]:
            """
            Obtiene a dos individuos de la población actual para que
            sean padres para este método de seleción.

            Returns:
                Una tupla de cromosomas en la pobleción a tratar como
                padres.
            """

            def roulette() -> int:
                total_fitness = sum(
                    (1 / (1 + individual.fitness_calc()))
                    for individual in self.__current_population
                )
                selection_point = random.uniform(0, total_fitness)
                cumulative_fitness = 0
                selected_index = 0
                for i in range(len(self.__current_population)):
                    cumulative_fitness += (1 / (1 + self.__current_population[i].fitness_calc()))
                    if cumulative_fitness >= selection_point:
                        selected_index = i
                        break
                return selected_index

            first_index = roulette()
            second_index = roulette()
            while first_index == second_index: second_index = roulette()
            return (
                self.__current_population[first_index],
                self.__current_population[second_index]
            )

        def generate_feasible_child():
            """
            Genera un hijo factible con padres seleccionados por torneo.
            """

            nonlocal time_without_feasible
            parents = get_parents()
            child = self.__generate_child(parents[0], parents[1])
            time_without_feasible += 1
            if child.feasible and child not in new_population:
                time_without_feasible -= self._time_constant\
                    if time_without_feasible >= self._time_constant else 0
                return child

        limit = False
        lock = RLock()
        time_without_feasible = 0
        new_population: List[Chromosome] = []
        with ThreadPoolExecutor(max_workers=THREADS) as executor:
            while len(new_population) < self.__population_size and not limit:
                limit = time_without_feasible >= self.__max_time_without_feasible
                future = executor.submit(generate_feasible_child)
                solution = future.result()
                with lock:
                    if (solution != None and solution not in new_population
                            and len(new_population) < self.__population_size):
                        new_population.append(solution)

        return new_population

    def next_generation(self) -> None:
        """
        Genera una nueva generación de individuos tomando como padres
        de estos a la generación anterior y la sustituye.
        """

        next_population: List[Chromosome] = []
        if self.__selection_method == 'tournament':
            next_population = self.__tournament_selection(tournament_size)
        elif self.__selection_method == 'elitism':
            next_population = self.__elitism_selection(
                elite_percentage, elite_children_percentage
            )
        elif self.__selection_method == 'roulette':
            next_population = self.__roulette_wheel_selection()
        self.__current_population = sorted(
            next_population, key=lambda solution: solution.fitness_calc()
        )

        for i, solution in enumerate(self.__current_population):
            if random.uniform(0, 100) > self.__mutation_rate: continue
            mutation = solution.mutate(
                self.__current_population[-1].fitness_calc()
            )
            if mutation is None or mutation in self.__current_population:
                continue
            if self.__always_mut: self.__current_population[i] = mutation
            else:
                self.__current_population.pop()
                self.__current_population.insert(0, mutation)

        self.__current_population = sorted(
            self.__current_population,
            key=lambda solution: solution.fitness_calc()
        )
        if self.__best_individual.fitness_calc()\
                > self.__current_population[0].fitness_calc():
            self.__best_individual = self.__current_population[0]

    @DeprecationWarning
    def start_genetic_algorithm(self) -> Chromosome:
        """
        Comienza las iteraciones del algoritmo genético.

        Returns:
            El individuo mejor adaptado de todas las generaciones.
        """

        self.first_generation()
        for i in range(self.__max_iterations):
            self.next_generation()
        return self.__best_individual

    def __str__(self) -> str:
        """
        Obtiene una representación en cadena de la población actual.

        Return:
            Una cadena que enumera a los individuos.
        """

        string_list = []
        for i in range(len(self.__current_population)):
            string_list.append(
                f'Indivuduo {i+1}: {str(self.__current_population[i])}'
            )
        return '\n'.join(string_list)

# http://web.cba.neu.edu/~msolomon/problems.htm

"""
#http://web.cba.neu.edu/~msolomon/c101.htm
#https://www.sintef.no/contentassets/adf48e65e3a84dd6871eb7586707675d/c101.txt
#https://www.sintef.no/projectweb/top/vrptw/100-customers/
# El vehículo es pensado para tener 200 de capacidad
C101 = pd.read_csv('C101.csv')
routes = [
    81, 78, 76, 71, 70, 73, 77, 79, 80,
    57, 55, 54, 53, 56, 58, 60, 59,
    98, 96, 95, 94, 92, 93, 97, 100, 99,
    32, 33, 31, 35, 37, 38, 39, 36, 34,
    13, 17, 18, 19, 15, 16, 14, 12,
    90, 87, 86, 83, 82, 84, 85, 88, 89, 91,
    43, 42, 41, 40, 44, 46, 45, 48, 51, 50, 52, 49, 47,
    67, 65, 63, 62, 74, 72, 61, 64, 68, 66, 69,
    5, 3, 7, 8, 10, 11, 9, 6, 4, 2, 1, 75,
    20, 24, 25, 27, 29, 30, 28, 26, 23, 22, 21
]
"""

"""
#http://web.cba.neu.edu/~msolomon/c201.htm
#https://www.sintef.no/contentassets/adf48e65e3a84dd6871eb7586707675d/c201.txt
#https://www.sintef.no/projectweb/top/vrptw/100-customers/
# El vehículo es pensado para tener 700 de capacidad
C201 = pd.read_csv('C201.csv')
routes = [
    93, 5, 75, 2, 1, 99, 100, 97, 92, 94, 95, 98, 7, 3, 4, 89, 91, 88, 84, 86, 83, 82, 85, 76, 71, 70, 73, 80, 79, 81, 78, 77, 96, 87, 90,
    67, 63, 62, 74, 72, 61, 64, 66, 69, 68, 65, 49, 55, 54, 53, 56, 58, 60, 59, 57, 40, 44, 46, 45, 51, 50, 52, 47, 43, 42, 41, 48,
    20, 22, 24, 27, 30, 29, 6, 32, 33, 31, 35, 37, 38, 39, 36, 34, 28, 26, 23, 18, 19, 16, 14, 12, 15, 17, 13, 25, 9, 11, 10, 8, 21
]
"""

"""
#http://web.cba.neu.edu/~msolomon/r101.htm
#https://www.sintef.no/contentassets/adf48e65e3a84dd6871eb7586707675d/r101.txt
#https://www.sintef.no/projectweb/top/vrptw/100-customers/
# El vehículo es pensado para tener 200 de capacidad
R101 = pd.read_csv('R101.csv')
routes = [
    2, 21, 73, 41, 56, 4,
    5, 83, 61, 85, 37, 93,
    14, 44, 38, 43, 13,
    27, 69, 76, 79, 3, 54, 24, 80,
    28, 12, 40, 53, 26,
    30, 51, 9, 66, 1,
    31, 88, 7, 10,
    33, 29, 78, 34, 35, 77,
    36, 47, 19, 8, 46, 17,
    39, 23, 67, 55, 25,
    45, 82, 18, 84, 60, 89,
    52, 6,
    59, 99, 94, 96,
    62, 11, 90, 20, 32, 70,
    63, 64, 49, 48,
    65, 71, 81, 50, 68,
    72, 75, 22, 74, 58,
    92, 42, 15, 87, 57, 97,
    95, 98, 16, 86, 91, 100
]
"""

"""
#http://web.cba.neu.edu/~msolomon/r201.htm
#https://www.sintef.no/contentassets/adf48e65e3a84dd6871eb7586707675d/r208.txt
#https://www.sintef.no/projectweb/top/vrptw/100-customers/
# El vehículo es pensado para tener 1000 de capacidad
# Este es el único que no da la distancia obtenida por los autores,
# pero la ruta es separada correctamente
R208 = pd.read_csv('R208.csv')
routes = [
    27, 52, 7, 88, 31, 10, 62, 11, 63, 90, 32, 30, 70, 69, 1, 50, 76, 12, 26, 28, 53, 2, 57, 42, 87, 97, 59, 99, 5, 84, 17, 45, 8, 46, 47, 36, 49, 64, 19, 48, 82, 18, 83, 60, 93, 91, 100, 37, 95, 94, 13,
    89, 6, 96, 92, 98, 85, 61, 16, 86, 44, 38, 14, 43, 15, 41, 22, 75, 56, 23, 67, 39, 24, 29, 79, 33, 81, 9, 51, 20, 66, 65, 71, 35, 34, 78, 3, 77, 68, 80, 54, 55, 25, 4, 72, 74, 73, 21, 40, 58
]
"""

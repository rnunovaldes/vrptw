"""Crossover Methods

Módulo que tiene varias funciones que sirven para crear tuplas al
cruzar dos tuplas distintas en una sola.
"""

import random
from typing import List, Tuple

__debug = False

def __has_unique_elements(numbers: Tuple[int, ...]) -> bool:
    """
    Una función de debug para los cruzamientos. Se comprueba que las
    tuplas tengan cada elemento una sola vez.

    Args:
        numbers (Tuple[int, ...]): Una tupla de números.

    Return:
        Si todos los elementos de la tupla son únicos.
    """

    return len(numbers) == len(set(numbers))

def one_point_crossover(
        parent_solution_1: Tuple[int, ...], parent_solution_2: Tuple[int, ...],
    ) -> Tuple[Tuple[int], Tuple[int]]:
    """
    Función para hacer cruzamiento en un punto aleatorio dados dos
    tuplas padres. Se generan dos tuplas.

    Args:
        parent_solution_1 (Tuple[int, ...]): Tupla que será usada para
            la nueva tupla al copiar los elementos de su índice 0 al n
            (un punto aleatorio).
        parent_solution_2 (Tuple[int, ...]): Tupla que será usada para
            la nueva tupla al copiar los elementos que no hayan sido
            copiados de parent_solution_1, comenzando desde el índice
            n+1 y comportándose como una lista circular de ser
            requerido.

    Returns:
        Una dupla de tuplas generadas a partir del cruce de dos tuplas.
    """

    def crossover(
            first_list: List[int], second_list: List[int], point: int
        ) -> List[int]:
        """
        Genera una lista nueva a partir de la combinación de dos listas
        """

        slide_1 = first_list[0:point+1]
        slide_2 = second_list[-point:] + second_list[:-point]
        child = slide_1.copy()
        i = 0
        while len(child) < len(first_list):
            x = slide_2[i]
            if x not in child:
                child.append(x)
            i += 1
        return tuple(child)

    random_index = random.randint(0, len(parent_solution_1) - 1)
    child_1 = crossover(list(parent_solution_1), list(parent_solution_2), random_index)
    child_2 = crossover(list(parent_solution_2), list(parent_solution_1), random_index)
    if __debug:
        if not __has_unique_elements(child_1): print(child_1)
        if not __has_unique_elements(child_2): print(child_2)
    return (child_1, child_2)

def order_crossover(
        parent_solution_1: Tuple[int, ...], parent_solution_2: Tuple[int, ...],
    ) -> Tuple[Tuple[int, ...], Tuple[int, ...]]:
    """
    Función para hacer cruzamiento escogiendo dos puntos para mantener
    una parte del padre y rellenar el resto con elementos de la otra
    tuplas no usados aún. Se generan dos tuplas.

    Args:
        parent_solution_1(Tuple[int, ...]): Lista que será usada para
            crear una nueva tupla al copiar los elementos de su índice
            0 al n (un punto aleatorio).
        parent_solution_2 (Tuple[int, ...]): Tupla que será usada para
            crear una nueva tupla al copiar los elementos que no hayan
            sido copiados de parent_solution_1, comenzando desde el
            índice n+1 y comportándose como una lista circular de ser
            requerido.

    Returns:
        Una dupla de tuplas generadas a partir del cruce de dos tuplas.
    """

    def crossover(
            first_list: List[int], second_list: List[int],
            point_1: int, point_2: int
        ) -> List[int]:
        """
        Genera una lista nueva a partir de la combinación de dos listas
        """
        slide = first_list[point_1:point_2+1]
        child = first_list.copy()
        parent_point = (point_2 + 1) % len(child)
        for i in range(len(child) - len(slide)):
            child_point = (i + point_2 + 1) % len(child)
            loop = True
            while loop:
                x = second_list[parent_point]
                if x not in slide:
                    child[child_point] = x
                    slide.append(x)
                    loop = False
                parent_point = (parent_point + 1) % len(child)
        return tuple(child)

    first_index = random.randint(0, len(parent_solution_1) - 2)
    second_index = random.randint(first_index, len(parent_solution_1) - 1)
    child_1 = crossover(list(parent_solution_1), list(parent_solution_2), first_index, second_index)
    child_2 = crossover(list(parent_solution_2), list(parent_solution_1), first_index, second_index)
    if __debug:
        if not __has_unique_elements(child_1): print(child_1)
        if not __has_unique_elements(child_2): print(child_2)
    return (child_1, child_2)

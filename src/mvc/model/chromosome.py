"""Chromosome

Módulo con una clase que modela una solución para VRPTW y calcula que
tan buena es.

También tiene una excepción personalizada usada para comprobaciones por
la clase Chromosome.
"""

import random
from typing import Callable, List, Tuple, Type
from pandas import DataFrame, Series

class NotPossibleRandomError(Exception):
    """
    Excepción personalizada para indicar que no se pueden generar dos
    índices aleatorios según una serie de condiciones.
    """

    def __init__(self, message: str):
        super().__init__(message)

class Chromosome():
    """
    Clase que representa a un individuo del algoritmo genético para el
    VRPTW y puede calcular su aptitud y mutaciones.

    Attributes:
        __clients_data: La información de los clientes.
        __max_capacity: La cantidad máxima de productos que los
            vehículos pueden llevar por ruta.
        __max_operation_time: El tiempo máximo que puede el vehículo
            permanecer en la ruta antes de tener que volver al
            depósito.
        __solution: Una lista de enteros que representa una posible
            solución del problema donde cada número es el cliente a
            entregar el pedido y están en orden de visita.
        __routes: Las rutas que se necesitan para solucionar el
            problema según solution.
        __total_distance: La distancia del recorrido, en conjunto de
            todas las rutas.
        __total_time: El tiempo en conjunto que toma recorrer todas las
            rutas. Similar a la distancia, pero toma en cuenta los
            tiempos muertos donde el vehículo espera a ser atendido y
            a despachar.
        __fitness_function: Una función que se va a usar para calcular
            un valor numérico de las soluciones.
        __vehicle_info: Una tupla que tiene dos valores enteros para
            registrar cuántos vehículos son lo mínimo y máximo que se
            dispone para generar las rutas.

    Methods:
        calculate_solution: Convierte una lista de enteros en una
            solución plausible para el VRPTW o indica que no lo es.
        fitness_calc: Obtiene un valor numérico que indica que tan bien
            adaptado es el individuo. Entre más pequeño, mejor.
        mutate: Genera un individuo nuevo basándose en el individuo
            actual.
    """

    def __init__(
            self, clients_data: DataFrame,
            max_capacity: int, vehicle_info: Tuple[int, int],
            max_operation_time: float,
            fitness_function: Callable[
                [float, Tuple[int, int, int], float], float
            ],
            solution: Tuple[int], always_mut: bool,
            break_point: List[int] | None = None
        ):
        """
        Constructor para representar un individuo.

        Parameters:
            clients_data (DataFrame): Información sobre los clientes.
            max_capacity (int): La cantidad máxima de carga que puede
                llevar el vehículo por ruta.
            vehicle_info (Tuple[int, int]): Una tupla con valores
                enteros para indicar cuántos vehículos son lo mínimo y
                lo máximo para usar en la solución.
            max_operation_time (float): El tiempo máximo que puede el
                vehículo permanecer en la ruta antes de tener que
                volver al depósito.
            fitness_function (Callable): Una función para crear valores
                numéricos dado un indivíduo de la población.
            solution (set[int]): Una lista de enteros que representa
                una posible solución del problema donde cada número es
                el cliente a entregar el pedido y están en orden.
        """

        self.__clients_data = clients_data
        self.__max_capacity = max_capacity
        self.__vehicle_info = vehicle_info
        self.__max_operation_time = max_operation_time
        self.__solution: Tuple[int] = solution
        self.__routes: List[List[int]]= []
        self.__route_distance: List[int] = [0]
        self.__total_distance: float = 0
        self.__total_time: float = 0
        self.__fitness_function: Callable[
            [float, Tuple[int, int, int], float], float
        ] = fitness_function
        self.__is_feasible = False
        self.__always_mut = always_mut
        if(break_point == None): break_point = [float('inf')]

        def calculate_distance(
                x1: int, x2: int, y1: int, y2: int
            ) -> float:
            """
            Calcula la distancia entre dos puntos.

            Args:
                x1 (int): La coordenada en el eje x del 1er punto.
                x2 (int): La coordenada en el eje x del 2do punto.
                y1 (int): La coordenada en el eje y del 1er punto.
                y2 (int): La coordenada en el eje y del 2do punto.

            Returns:
                La distancia que hay entre ambos puntos.
            """

            return ((x2 - x1) ** 2 + (y2 - y1) ** 2) ** 0.5

        def calculate_arrival_time(
                previous_arrival_time: float,
                distance_between_clients: float,
                previous_client_index: int, current_client_index: int
            ) -> float:
            """
            Calcula el tiempo de llegada del vehículo de un cliente
            a otro.

            Args:
                previous_arrival_time (float): El tiempo que lleva
                    el vehículo en la ruta.
                distance_between_clients (float): La distanca que
                    hay entre ambos clientes
                previous_client_index: El índice en el dataframe del
                    cliente en el que se está.
                current_client_index: El índice en el dataframe del
                    cliente al que se quiere llegar.

            Returns:
                El tiempo en el que el vehículo llegará con el
                cliente.
            """

            earlier_time: int = self.__clients_data.loc[
                current_client_index, 'READY TIME'
            ]
            service_time: int = self.__clients_data.loc[
                previous_client_index, 'SERVICE TIME'
            ]

            return max(
                earlier_time,
                previous_arrival_time + service_time
                    + distance_between_clients
            )

        def calculate_max_waiting_time(
                previous_arrival_time: float,
                distance_between_clients: float,
                previous_client_index: int, current_client_index: int
            ) -> float:
            """
            Calcula cuánto tiempo puede esperar el vehículo a un
            cliente si este no está disponible aún.

            Args:
                previous_arrival_time (float): El tiempo que lleva
                    el vehículo en la ruta.
                distance_between_clients (float): La distanca que
                    hay entre ambos clientes
                previous_client_index: El índice en el dataframe del
                    cliente en el que se está.
                current_client_index: El índice en el dataframe del
                    cliente al que se quiere llegar.

            Returns:
                Cuanto tiempo que tendrá que esperar el vehículo
                para que el cliente pueda ser despachado.
            """

            earlier_time: int = self.__clients_data.loc[
                current_client_index, 'READY TIME'
            ]
            service_time: int = self.__clients_data.loc[
                previous_client_index, 'SERVICE TIME'
            ]
            return max(
                0,
                earlier_time - previous_arrival_time
                    - service_time - distance_between_clients
            )

        # (True) Indica si el viaje se hace del deposito al cliente o
        # (False) del cliente anterior
        new_route = True
        deposit: Series[int] = self.__clients_data.loc[0]
        # Productos que tiene actualmente el vehículo
        capacity = self.__max_capacity
        # Tiempo de viaje para la ruta actual, contando esperas
        on_route_time = 0
        previous_arrival_time = 0

        # Si llega a 2 significa que es imposible cumplir la ruta tanto
        # del cliente aterior al actual y
        # del cliente anterior al depósito y del depósito al actual
        repeatly_run_out = 0
        repeatly_times_late = 0
        # La ruta recorrida actualmente
        route = []
        i = 0
        while i < len(self.__solution):
            client_index: int = self.__solution[i]
            previous_client_index: int = self.__solution[i - 1]
            if new_route: previous_client_index = 0

            client: Series[int] = self.__clients_data.loc[client_index]
            previous_client: Series[int] =\
                self.__clients_data.loc[previous_client_index]

            # Viaje del cliente al anterior al actual
            travel_time = calculate_distance(
                previous_client['XCOORD.'], client['XCOORD.'],
                previous_client['YCOORD.'], client['YCOORD.']
            )

            arrival_time = calculate_arrival_time(
                previous_arrival_time, travel_time,
                previous_client_index, client_index
            )
            waiting_time = calculate_max_waiting_time(
                previous_arrival_time, travel_time,
                previous_client_index, client_index
            )
            previous_arrival_time = arrival_time

            # Viaje del cliente al deposito
            last_travel_time = calculate_distance(
                client['XCOORD.'], deposit['XCOORD.'],
                client['YCOORD.'], deposit['YCOORD.']
            )

            capacity -= client['DEMAND']
            on_route_time += travel_time + waiting_time

            current_time: float = on_route_time + client['SERVICE TIME']
            past_service_time: bool = on_route_time > client['DUE DATE']
            cant_go_back = (
                current_time + last_travel_time > self.__max_operation_time
                    or current_time + last_travel_time > deposit['DUE DATE']
            )
            if ((repeatly_run_out == 2 and capacity < 0)
                    or (new_route and cant_go_back)
                    or (repeatly_times_late == 2 and past_service_time)):
                return
            if capacity < 0 or cant_go_back or past_service_time or break_point[0] == previous_client_index:
                if break_point[0] == previous_client_index: break_point.pop(0)
                if capacity < 0 : repeatly_run_out += 1
                elif past_service_time:
                    repeatly_times_late += 1
                new_route = True
                last_travel_time = calculate_distance(
                    previous_client['XCOORD.'], deposit['XCOORD.'],
                    previous_client['YCOORD.'], deposit['YCOORD.']
                )
                self.__total_distance += last_travel_time
                self.__total_time += current_time + last_travel_time
                self.__routes.append(route)
                self.__route_distance.append(
                    self.__total_distance - sum(self.__route_distance)
                )
                route = []
                capacity = self.__max_capacity
                on_route_time = 0
                i -= 1
            else:
                new_route = False
                repeatly_run_out = 0
                repeatly_times_late = 0
                on_route_time = current_time
                self.__total_distance += travel_time
                route.append(client_index)
                if i == len(self.__solution) - 1:
                    self.__total_distance += last_travel_time
                    self.__total_time += current_time + last_travel_time
                    self.__routes.append(route)
                    self.__route_distance.append(
                        self.__total_distance - sum(self.__route_distance)
                    )
            i += 1
        if len(self.__routes) <= self.__vehicle_info[1]: self.__is_feasible = True

    @property
    def feasible(self) -> bool:
        """
        Indica si la solución propuesta es plausible. Se debe usar
        primero calculate_solution.

        Returns:
            Si esta solución es plausible.
        """

        return self.__is_feasible

    @property
    def solution(self) -> Tuple[int]:
        """
        Un getter para el orden de los clientes.

        Returns:
            Una tupla con el orden de los clientes.
        """

        return self.__solution

    @property
    def total_distance(self) -> float:
        """
        Un getter para obtener la distancia recorrida en las rutas.

        Returns:
            Distancia recorrida como un número flotante.
        """

        return self.__total_distance

    @property
    def total_time(self) -> float:
        """
        Un getter para obtener el tiempo requerido para recorrer las
        rutas.

        Returns:
            Tiempo para recorrer las rutas contando tiempos de espera.
        """

        return self.__total_time

    @property
    def routes(self) -> List[List[int]]:
        """
        Un getter para obtener las rutas.

        Returns:
            Todas las rutas necesarias para visitar a los clientes.
        """

        return self.__routes

    def fitness_calc(self) -> float:
        """
        Hace el cálculo de la función de aptitud y regresa ese número.
        Entre más pequeño sea, mejor es la solución.

        Returns:
            Una representación numérica de qué tan buena es la
            solución.
        """

        if self.__is_feasible:
            return self.__fitness_function(
                self.__total_distance,
                (len(self.__routes), self.__vehicle_info[0], self.__vehicle_info[1]),
                self.__total_time, self.__route_distance
            )
        return float('inf')

    def mutate(self, worst_fitness: float) -> Type['Chromosome'] | None:
        """
        Genera o no una mutación en base a un porcentaje dado al
        incializar la clase. Una serie de 9 mutaciones son probadas en
        orden hasta que una mutación mejore la solución o se hayan
        probado las 9, entonces se devuelve esa solución como un nuevo
        cromosoma.

        Args:
            worst_fintess: El valor a superar para que la mutación sea
                de vuelta.

        Returns:
            Un individuo que mejora la solución al mutar o Nada si no
            es posible obtener una mejora con la mutación
        """

        def get_two_random_indexes(
                in_same_route: bool | None=None
            ) -> Tuple[int, int]:
            """
            Función para obtener dos números aleatorios que no sean
            iguales entre ellos y puedan ser usados como índices en la
            solución de este cromosoma.

            Args:
                in_same_route (bool): Si ambos indices deben estar en
                    la misma ruta.

            Returns:
                Dos números que son los índices escogidos al azar del
                arreglo de clientes.
            """

            def get_two_random_ints(start: int, end: int) -> Tuple[int, int]:
                first_index = random.randint(start, end)
                second_index = random.randint(start, end)
                while first_index == second_index:
                    second_index = random.randint(start, end)
                if first_index > second_index:
                    first_index, second_index = second_index, first_index
                return (first_index, second_index)
            start = 0
            end = len(self.__solution)-1
            if in_same_route:
                possible_route_indexes: List[int] = []
                for i in range(len(self.__routes)):
                    if len(self.__routes[i]) >= 3:
                        possible_route_indexes.append(i)
                if len(possible_route_indexes) == 0:
                    raise NotPossibleRandomError(
                        'No hay una ruta con tres o más clientes'
                    )
                route_index = random.sample(possible_route_indexes, 1)[0]
                for i in range(route_index):
                    start += len(self.__routes[i])
                end = start + len(self.__routes[route_index])
                u, v = get_two_random_ints(start, end-1)
                if u+1 == v:
                    if v+1 == end: u -= 1
                    else: v += 1
                if v+1 == end:
                    v -= 1
                    if u+1 == v: u -= 1
                return (u, v)
            elif in_same_route == False:
                number_of_routes = len(self.__routes)
                if number_of_routes == 1:
                    raise NotPossibleRandomError('Solo hay una ruta')
                first_route, second_route = get_two_random_ints(
                    0, number_of_routes-1
                )
                for i in range(first_route):
                    start += len(self.__routes[i])
                end = start + len(self.__routes[first_route]) - 1
                u = random.randint(start, end)
                for i in range(first_route, second_route):
                    start += len(self.__routes[i])
                end = start + len(self.__routes[second_route]) - 1
                v = random.randint(start, end)
                if u+1 == v:
                    if v+1 == len(self.__solution): u -= 1
                    else: v += 1
                return (u, v)
            else: return get_two_random_ints(start, end)

        def m1() -> List[int]:
            """
            Para la secuencia (..., u,... ,v ,...) entonces hacer
            (..., v, u, ...).

            Returns:
                Una lista con los índices ordenados de acuedo a la
                mutación de la solución de este cromosoma.
            """

            u_index, v_index = get_two_random_indexes()
            new_solution: List[int] = list(self.__solution)
            for i in range(u_index, v_index):
                new_solution[i] = self.__solution[i+1]
            new_solution[v_index] = self.__solution[u_index]
            return new_solution

        def m2() -> List[int]:
            """
            Para la secuencia (..., u, x, ..., v, ...) entonces hacer
            (..., v, u, x, ...).

            Returns:
                Una lista con los índices ordenados de acuedo a la
                mutación de la solución de este cromosoma.
            """

            u_index, v_index = get_two_random_indexes()
            new_solution: List[int] = list(self.__solution)
            if u_index+1 == v_index:
                if v_index+1 == len(self.__solution):
                    u_index -= 1
                    if u_index < 0: return new_solution
                else: v_index += 1
            for i in range(u_index, v_index-1):
                new_solution[i] = self.__solution[i+2]
            new_solution[v_index-1] = self.__solution[u_index]
            new_solution[v_index] = self.__solution[u_index+1]
            return new_solution

        def m3() -> List[int]:
            """
            Para la secuencia (..., u, x, ..., v, ...) entonces hacer
            (..., v, x, u, ...).

            Returns:
                Una lista con los índices ordenados de acuedo a la
                mutación de la solución de este cromosoma.
            """

            u_index, v_index = get_two_random_indexes()
            new_solution: List[int] = list(self.__solution)
            if u_index+1 == v_index:
                if v_index+1 == len(self.__solution):
                    u_index -= 1
                    if u_index < 0: return new_solution
                else: v_index += 1
            for i in range(u_index, v_index-1):
                new_solution[i] = self.__solution[i+2]
            new_solution[v_index-1] = self.__solution[u_index+1]
            new_solution[v_index] = self.__solution[u_index]
            return new_solution

        def m4() -> List[int]:
            """
            Para la secuencia (..., u, ..., v, ...) entonces hacer
            (..., v, ..., u, ...).

            Returns:
                Una lista con los índices ordenados de acuedo a la
                mutación de la solución de este cromosoma.
            """

            u_index, v_index = get_two_random_indexes()
            new_solution: List[int] = list(self.__solution)
            new_solution[u_index] = self.__solution[v_index]
            new_solution[v_index] = self.__solution[u_index]
            return new_solution

        def m5() -> List[int]:
            """
            Para la secuencia (..., u, x, ..., v, ...) entonces hacer
            (..., v, ..., u, x, ...).

            Returns:
                Una lista con los índices ordenados de acuedo a la
                mutación de la solución de este cromosoma.
            """

            new_solution: List[int] = list(self.__solution)
            u_index, v_index = get_two_random_indexes()
            if u_index+1 == v_index:
                if v_index+1 == len(self.__solution):
                    u_index -= 1
                    if u_index < 0: return new_solution
                else: v_index += 1
            new_solution[u_index] = self.__solution[v_index]
            for i in range(u_index+1, v_index-1):
                new_solution[i] = self.__solution[i+1]
            new_solution[v_index-1] = self.__solution[u_index]
            new_solution[v_index] = self.__solution[u_index+1]
            return new_solution

        def m6() -> List[int]:
            """
            Para la secuencia (..., u, x, ..., v, y, ...) entonces
            hacer (... , v, y, ..., u, x, ...).

            Returns:
                Una lista con los índices ordenados de acuedo a la
                mutación de la solución de este cromosoma.
            """

            u_index, v_index = get_two_random_indexes()
            new_solution: List[int] = list(self.__solution)
            if u_index+1 == v_index:
                if v_index+1 == len(self.__solution):
                    u_index -= 1
                    if u_index < 0: return new_solution
                else: v_index += 1
            if v_index+1 == len(self.__solution):
                v_index -= 1
                if u_index+1 == v_index:
                    u_index -= 1
                    if u_index < 0: return new_solution
            new_solution[u_index] = self.__solution[v_index]
            new_solution[u_index+1] = self.__solution[v_index+1]
            new_solution[v_index] = self.__solution[u_index]
            new_solution[v_index+1] = self.__solution[u_index+1]
            return new_solution

        def m7() -> List[int]:
            """
            Para la secuencia (..., u, x, ..., v, y, ...) donde u y v
            están en la misma ruta entonces hacer
            (..., u, v, ..., x, y, ...).

            Returns:
                Una lista con los índices ordenados de acuedo a la
                mutación de la solución de este cromosoma.
            """

            new_solution: List[int] = list(self.__solution)
            try:
                u_index, v_index = get_two_random_indexes(True)
                new_solution[u_index+1] = self.__solution[v_index]
                new_solution[v_index] = self.__solution[u_index+1]
            except NotPossibleRandomError: pass
            finally: return new_solution

        def m8() -> List[int]:
            """
            Para la secuencia (..., u, x, ..., v, y, ...) donde u y v
            están en distintas rutas entonces hacer
            (..., u, v, ..., x, y, ...).

            Returns:
                Una lista con los índices ordenados de acuedo a la
                mutación de la solución de este cromosoma.
            """

            new_solution: List[int] = list(self.__solution)
            try:
                u_index, v_index = get_two_random_indexes(False)
                if u_index < 0: return new_solution
                new_solution[u_index+1] = self.__solution[v_index]
                new_solution[v_index] = self.__solution[u_index+1]
            except NotPossibleRandomError: pass
            return new_solution

        def m9() -> List[int]:
            """
            Para la secuencia (... , u, x, ..., v, y, ...) donde u y v
            están en distintas rutas entonces hacer
            (..., u, y, ..., x, v, ...).

            Returns:
                Una lista con los índices ordenados de acuedo a la
                mutación de la solución de este cromosoma.
            """

            new_solution: List[int] = list(self.__solution)
            try:
                u_index, v_index = get_two_random_indexes(False)
                if u_index < 0: return new_solution
                if v_index+1 == len(self.__solution):
                    v_index -= 1
                    if u_index+1 == v_index:
                        u_index -= 1
                        if u_index < 0: return new_solution
                new_solution[u_index+1] = self.__solution[v_index+1]
                new_solution[v_index] = self.__solution[u_index+1]
                new_solution[v_index+1] = self.__solution[v_index]
            except NotPossibleRandomError: pass
            return new_solution

        mutations = [m1, m2, m3, m4, m5, m6, m7, m8, m9]
        if self.__always_mut:
            available_mutations = mutations[:]
            while available_mutations:
                selected_mutation = random.choice(available_mutations)
                available_mutations.remove(selected_mutation)
                mutation = Chromosome(
                    self.__clients_data,
                    self.__max_capacity,
                    self.__vehicle_info,
                    self.__max_operation_time,
                    self.__fitness_function,
                    tuple(selected_mutation()),
                    self.__always_mut
                )
                if mutation.feasible: return mutation
        else:
            for mutation_func in mutations:
                mutation = Chromosome(
                    self.__clients_data,
                    self.__max_capacity,
                    self.__vehicle_info, self.__max_operation_time,
                    self.__fitness_function,
                    tuple(mutation_func()),
                    self.__always_mut
                )
                if mutation.fitness_calc() < worst_fitness: return mutation
        return None

    def __eq__(self, other_chromosome: object) -> bool:
        """
        Compara si este objeto es igual a otro.

        Args:
            other_chromosome (object): Un objeto que se va a comparar
            si es Chromosome y tienen la misma solución.

        Returns:
            Si los objetos efectivamente son iguales.
        """
        if isinstance(other_chromosome, Chromosome):
            return self.solution == other_chromosome.solution
        return False

    def __hash__(self) -> int:
        """
        Obtiene un número único para el cromosoma en base a su
        solución.
        """

        return hash(self.solution)

    def __str__(self) -> str:
        """
        Representación como cadena del cromosoma.

        Returns:
            Una cadena que tiene la distancia del recorrido, el tiempo
            empleado, la cantidad de rutas necesarias y los clientes en
            cada ruta.
        """

        if not self.__is_feasible: return 'No es una solución'
        format_str = 'Distancia: {:.2f}, Tiempo: {:.2f}, #Rutas: {:d}\n'
        format_str = format_str.format(
            self.__total_distance, self.__total_time, len(self.__routes)
        )
        formatted_routes_list = []
        for i, subroute in enumerate(self.__routes, start=1):
            formatted_routes_list.append(f'Ruta {i}: {subroute}')
        format_routes = '\n'.join(formatted_routes_list)
        return (format_str + format_routes)

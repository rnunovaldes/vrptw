"""Clusterer

Módulo con una clase que puede organizar datos de clientes de
acuerdo a su distancia geográfica.
"""

import random
import pandas as pd

from typing import List, Tuple
from pandas import DataFrame, Series

def calculate_distance(x1: int, x2: int, y1: int, y2: int) -> float:
    """
    Calcula la distancia entre dos puntos.

    Args:
        x1 (int): La coordenada en el eje x del 1er punto.
        x2 (int): La coordenada en el eje x del 2do punto.
        y1 (int): La coordenada en el eje y del 1er punto.
        y2 (int): La coordenada en el eje y del 2do punto.

    Returns:
        La distancia que hay entre ambos puntos.
    """

    return ((x2 - x1) ** 2 + (y2 - y1) ** 2) ** 0.5


class Clusterer():
    """
    Clase que pude organizar datos de clientes con una implementación de
    un algoritmo de barrido de líneas

    Attributes:
        __clients_data: Tiene información sobre la ubicación de los
            clientes.
        __distance: Cuánta distancia hay entre las dos líneas de
            barrido.
        __clusters: Un diccionario de listas donde cada llave representa
            una agrupación y cada lista tiene como elementos los
            identificadores de los clientes que forman parte de la
            agrupación.
        __groups: Una lista del tamaño de la cantidad de clientes a
            agrupar que sirve para guardadr temporalmente los índices
            de los clientes que están en el mismo grupo

    Methods:
        organize: Empieza a organizar a los clientes de acuerdo a su
            ubicación geográfica con los parametros dados.
        generate_solutions: Crea listas de ordenamientos para los
            clientes siempre y cuando sean cercanos entre ellos.
        _new_clusters: Agrupa a los clientes suponiendo que desde un
            inicio no hay clientes previamente agrupados cercanos a los
            nuevos clientes.
        _resume_clustering: Agrupa a los clientes suponiendo que hay
            clientes previamente agrupados y que son cernanos a los
            nuevos clientes a agrupar.
        _finisher: Crea un diccionario para poder acceder al resultado
            de una forma organizada
    """

    def __init__(self, clients_data: DataFrame, distance: int):
        """
        Constructor para realizar el agrupamiento de clientes.

        Parameters:
            data (DataFrame): Toda la información de los clientes.
            distance (int): ULa distancia entre las dos líneas de
                barrido.
        """

        self.__clients_data = clients_data
        self.__distance = distance
        self.__clusters: dict[int, List[int]] = []
        self.__organize()

    def __organize(self) -> None:
        """
        Empieza a organizar a los clientes de acuerdo a su ubicación
        geográfica con los parametros dados en el constructor.
        """

        ysort: DataFrame =\
            self.__clients_data.iloc[1:].sort_values(['YCOORD.', 'XCOORD.'])

        self.__groups: List[int] = [-10 for _ in range(len(ysort))]

        ysort_min: int = ysort['YCOORD.'].min()
        af: DataFrame = ysort[
            (ysort['YCOORD.'] >= ysort_min)
                & (ysort['YCOORD.'] <= ysort_min + self.__distance)
        ]
        af = af.sort_values(['XCOORD.', 'YCOORD.'])
        self._new_clusters(af, self.__distance)

        for _ in range(len(ysort)):
            # print(af)
            y_min: int = af['YCOORD.'].min()
            temp_df: DataFrame = af[af['YCOORD.'] > y_min]
            # print(str(y_min) + '\n')
            if len(temp_df) == 0:
                temp_df = ysort[(ysort['YCOORD.'] > af['YCOORD.'].max())]
                if len(temp_df) == 0: break
                ysort_min = temp_df['YCOORD.'].min()
                af = temp_df[
                    (temp_df['YCOORD.'] >= ysort_min)
                        & (temp_df['YCOORD.'] <= ysort_min + self.__distance)
                ]
                af = af.sort_values(['XCOORD.', 'YCOORD.'])
                self._new_clusters(af, self.__distance)
            else:
                af = temp_df
                offset: int = self.__distance\
                    - af['YCOORD.'].max() + af['YCOORD.'].min()
                new_clients: DataFrame = ysort[
                    (ysort['YCOORD.'] > af['YCOORD.'].max())
                        & (ysort['YCOORD.'] <= af['YCOORD.'].max() + offset)
                ]
                if len(new_clients) == 0: continue
                new_clients = new_clients.sort_values(['XCOORD.', 'YCOORD.'])
                # print(af)
                # print(new_clients)
                self._new_clusters(new_clients, self.__distance)
                af = self._resume_clustering(
                    af, new_clients, self.__distance
                )
        self.__clusters = self._finisher()

    def _new_clusters(self, af: DataFrame, distance: int):
        """
        Función que agrupa a los clientes suponiendo que desde
        un inicio no hay clientes previamente agrupados que sean
        cercanos a los nuevos clientes.
        Empieza creando una nueva agrupación con el cliente cuyas
        coordenadas sean las menores en el eje y.
        Luego agrupa a los clientes subsecuentes de acuerdo a qué tan
        cernanos estén de su vecino cliente previo.

        Args:
            af (DataFrame): El frente de avance. Son un subconjuto de
                clientes que se forma de acuerdo a dos paralelas y
                ordenados en el eje x.
            distance (int): Es la distancia que hay entre las dos
                paralelas usadas para generar el subconjuto y tambien
                sirve para determinar si los clientes son vecinos
                cercanos
        """

        first_group: int = af.iloc[0]['CUST NO.'] - 2
        self.__groups[first_group] = first_group

        for i in range(1, len(af)):
            client: Series[int] = af.iloc[i]
            client_index: int = client['CUST NO.'] - 2
            subset = af.iloc[:i]
            near_clients = subset[
                (subset['XCOORD.'] >= client['XCOORD.'] - distance) &
                (subset['XCOORD.'] <= client['XCOORD.'])
            ]
            near_on_past: set[int] = set()

            for j in range(len(near_clients)):
                calc_distance = calculate_distance(
                    client['XCOORD.'], near_clients.iloc[j]['XCOORD.'],
                    client['YCOORD.'], near_clients.iloc[j]['YCOORD.']
                )
                if calc_distance <= distance:
                    near_on_past.add(
                        self.__groups[near_clients.iloc[j]['CUST NO.'] - 2]
                    )
            if near_on_past:
                min_value = min(near_on_past)
                self.__groups = [
                    min_value if g in near_on_past else g
                        for g in self.__groups
                ]
                self.__groups[client_index] = min_value
            else: self.__groups[client_index] = client_index

    def _resume_clustering(
            self, af: DataFrame, new_clients: DataFrame, distance: int
        ) -> DataFrame:
        """
        Función que agrupa a los clientes suponiendo que hay
        clientes previamente agrupados y que son cercanos a los nuevos
        clientes a agrupar.
        Comprueba si cada nuevo cliente puede ser agrupado en una
        agrupación existente y si no es posible, crea una nueva
        agrupación para él.

        Args:
            af (DataFrame): El frente de avance. Son un subconjunto de
                clientes que se forma de acuerdo a dos paralelas y
                ordenados en el eje x.
            new_clients (DataFrame): Nuevos clientes que no forman parte
                del frente de avance y se agregarán a agrupaciones
                existentes de ser posible.
            distance (int): Es la distancia que hay entre las dos
                paralelas usadas para generar el subconjunto y también
                sirve para determinar si los clientes son vecinos
                cercanos.

        Returns:
            Un nuevo conjunto de clientes que se crea al unir el frente
            de avance con los nuevos clientes que acaban de ser
            agrupados.
        """

        for i in range(len(new_clients)):
            client: Series[int] = new_clients.iloc[i]
            client_index: int = client['CUST NO.'] - 2
            near_clients = af[
                (af['XCOORD.'] >= client['XCOORD.'] - distance)
                    & (af['XCOORD.'] <= client['XCOORD.'] + distance)
            ]
            near_on_past = set([self.__groups[client_index]])
            for j in range(len(near_clients)):
                calc_distance = calculate_distance(
                    client['XCOORD.'], near_clients.iloc[j]['XCOORD.'],
                    client['YCOORD.'], near_clients.iloc[j]['YCOORD.']
                )
                if calc_distance <= distance:
                    near_on_past.add(
                        self.__groups[near_clients.iloc[j]['CUST NO.'] - 2]
                    )

            if len(near_on_past) > 1:
                min_value = min(near_on_past)
                self.__groups = [
                    min_value if g in near_on_past else g
                        for g in self.__groups
                ]
        af = pd.concat([af, new_clients])
        af = af.sort_values(['XCOORD.', 'YCOORD.'])
        return af

    def _finisher(self) -> dict[int, List[int]]:
        """
        Método estático que se encarga de poner en un diccionario la
        la información respecto de las agrupaciones basándose en una
        lista.
        Esta lista tiene tantos elementos como clientes que se agruparon
        y cada índice representa al identificador de un cliente.
        Los elementos en la lista representa a qué agrupación
        pertenecen.

        Returns:
            El diccionario de forma que es más accesible y legible la
            información de las agrupaciones de clientes.
        """

        clusters: dict[int, List[int]] = {}
        for index, element in enumerate(self.__groups):
            if element in clusters: clusters[element].append(index+1)
            else: clusters[element] = [index+1]
        return {
                new_key: clusters[old_key]
                    for new_key, old_key in enumerate(clusters)
            }

    def generate_solution(self) -> Tuple[int]:
        """
        Genera una tupla con los números de los clientes que sirve
        para crear una población incial para el algoritmo genético.
        """

        if not(self.__clusters): return None

        solution = []
        for lists in self.__clusters.values():
            random_list = lists[:]
            random.shuffle(random_list)
            solution.append(random_list)

        random.shuffle(solution)
        solution = [element for sublist in solution for element in sublist]

        return tuple(solution)


    @property
    def clusters(self) -> dict[int, List[int]]:
        """
        Un getter para obtener el resultado de la organización.

        Returns:
            Un diccionario que tiene listas con las agrupaciones de los
            clientes.
        """

        return self.__clusters

    def __str__(self):
        """
        Sirve para mostrar una representación en cadena de las
        agrupaciones de los clientes

        Returns:
            La cadena que representa al resultado.
        """
        strings: List[str] = []
        for id, clients in self.__clusters.items():
            string = f"{id:3n} : {clients}\n"
            strings.append(string)
        return ''.join(strings)

import random
from typing import Callable, List, Tuple

from pandas import DataFrame

from mvc.model.chromosome import Chromosome

tournament_size = 2
elite_percentage = 30
elite_children_percentage = 50

def valid_percentage(percentage: float) -> float:
    """
    Valida que un número sea un porcentaje y si lo es, lo regresa, en
    caso contrario, da el número más cercano de éste entre 0 y 100.

    Args:
        percentage (float): En número a limitar si es que es mayor a
            100 o menor a 0.
    """

    if percentage < 0: return 0
    elif percentage > 100: return 100
    else: return percentage

class Selection():

    def __init__(self, selection_method: str, data: DataFrame,
            crossover: Callable[
                [Tuple[int, ...], Tuple[int, ...]],
                Tuple[Tuple[int, ...], Tuple[int, ...]]
            ]
        ):

        self.__selection_method = selection_method
        self.__data = data
        self.__crossover = crossover
        self.__current_population = None

    @property
    def population(self) -> str:
        """
        """

        return self.__current_population

    @population.setter
    def population(self, new_population: List[Chromosome]):

        self.__current_population = new_population

    def create_new_population(self):

        next_population: List[Chromosome] = []
        if self.__selection_method == 'tournament':
            next_population = self.__tournament_selection(tournament_size)
        elif self.__selection_method == 'elitism':
            next_population = self.__elitism_selection(
                elite_percentage, elite_children_percentage
            )
        elif self.__selection_method == 'roulette':
            next_population = self.__roulette_wheel_selection()
        self.__current_population = sorted(
            next_population, key=lambda solution: solution.fitness_calc()
        )
        return sorted(
            next_population, key=lambda solution: solution.fitness_calc()
        )

    def __generate_child(
            self, parent_1: Chromosome, parent_2: Chromosome
        ) -> Chromosome:
        """
        Genera la descendencia de dos padres dados. Se crean dos hijos
        a partir de ellos y se devuelve al azar un de los dos.

        Args:
            parent_1 (Chromosome): El primer padre.
            parent_2 (Chromosome): El segundo padre.

        Returns:
            El hijo generado escogido del cruzamiento de los padres
        """

        children = self.__crossover(parent_1.solution, parent_2.solution)
        child_1 = Chromosome(
            self.__data, self.__max_capacity,
            self.__vehicle_info, self.__max_operation_time,
            self.__fitness, children[0]
        )
        child_2 = Chromosome(
            self.__data, self.__max_capacity,
            self.__vehicle_info, self.__max_operation_time,
            self.__fitness, children[1]
        )
        return min(
            child_1, child_2, key=lambda solution: solution.fitness_calc()
        )

    def __tournament_selection(
            self, tournament_size: int=2
        ) -> List[Chromosome]:
        """
        Selecciona dos individuos de una lista de los posibles padres
        por torneo. El torneo se realiza seleccionando una cantidad
        fija de individuos al azar y seleccionando al mejor adaptado de
        esta selección.

        Args:
            tournament_size (int): La cantidad de individuos a ser
                escogidos por ronda del torneo.

        Returns:
            Una lista que tiene a la descendencia creada con esta
            selección. Es del mismo tamaño que la población que la
            crea.
        """

        if tournament_size < 2: tournament_size = 2

        def get_parents() -> Tuple[Chromosome, Chromosome]:
            """
            Obtiene a dos individuos de la población actual para que
            sean padres para este método de seleción.

            Returns:
                Una tupla de cromosomas en la pobleción a tratar como
                padres.
            """

            first_index = min(
                random.sample(range(len(self.__current_population)), tournament_size)
            )
            second_index = min(
                random.sample(range(len(self.__current_population)), tournament_size)
            )
            while first_index == second_index:
                second_index = min(
                    random.sample(
                        range(len(self.__current_population)), tournament_size
                    )
                )
            return (
                self.__current_population[first_index],
                self.__current_population[second_index]
            )

        new_population: List[Chromosome] = []
        time_without_feasible = 0
        limit = False
        while len(new_population) < self.__population_size and not limit:
            limit = time_without_feasible >= self.__max_time_without_feasible
            parents = get_parents()
            child = self.__generate_child(parents[0], parents[1])
            time_without_feasible += 1
            if child.feasible and child not in new_population:
                new_population.append(child)
                time_without_feasible -= self._time_constant\
                    if time_without_feasible >= self._time_constant else 0

        return new_population

    def __elitism_selection(
            self, elite_percentage: float=0, elite_children_percentage: float=0
        ) -> List[Chromosome]:
        """
        Selecciona al azar a dos individuos élite en el porcentage
        dado. Si es 0 o 100 por ciento, es como escoger al azar en toda
        la población.

        Args:
            elite_percentage (float): El porcentage de individuos que
                serán considerados élite.
            elite_children_percentage (float): El porcentage de hijos
                que serán creados por los individuos élite.

        Returns:
            Una lista que tiene a la descendencia creada con esta
            selección. Es del mismo tamaño que la población que la
            crea.
        """

        elite_size = int(
            (len(self.__current_population) * valid_percentage(elite_percentage)) / 100
        )
        elite_children_size = int(
            (len(self.__current_population) * valid_percentage(elite_children_percentage)) / 100
        )

        def get_parents(limit: int) -> Tuple[Chromosome, Chromosome]:
            """
            Obtiene a dos individuos de la población actual para que
            sean padres para este método de seleción.

            Args:
                limit (int): Hasta que índice de la población se limita
                    la selección de padres para escoger solo un grupo
                    de élite.

            Returns:
                Una tupla de cromosomas en la pobleción a tratar como
                padres.
            """

            first_index = random.randint(0, limit)
            second_index = random.randint(0, limit)
            while first_index == second_index:
                second_index = random.randint(0, limit)
            return (
                self.__current_population[first_index],
                self.__current_population[second_index]
            )

        new_population: List[Chromosome] = []
        time_without_feasible = 0
        limit = False
        if elite_size > 1 and elite_children_size > 0:
            while(len(new_population) < elite_children_size and not limit):
                limit = time_without_feasible >= self.__max_time_without_feasible
                parents = get_parents(elite_size)
                child = self.__generate_child(parents[0], parents[1])
                time_without_feasible += 1
                if child.feasible and child not in new_population:
                    new_population.append(child)
                    time_without_feasible -= self._time_constant\
                        if time_without_feasible >= self._time_constant else 0
        time_without_feasible = 0
        limit = False
        while len(new_population) < self.__population_size and not limit:
            limit = time_without_feasible >= self.__max_time_without_feasible
            parents = get_parents(len(self.__current_population)-1)
            child = self.__generate_child(parents[0], parents[1])
            time_without_feasible += 1
            if child.feasible and child not in new_population:
                new_population.append(child)
                time_without_feasible -= self._time_constant\
                    if time_without_feasible >= self._time_constant else 0
        return new_population

    def __roulette_wheel_selection(self) -> List[Chromosome]:
        """
        Selecciona por ruleta a dos individuos. Entre más apto sea el
        individuo, más probable es que sea seleccionado.

        Returns:
            Una lista que tiene a la descendencia creada con esta
            selección. Es del mismo tamaño que la población que la
            crea.
        """

        def get_parents() -> Tuple[Chromosome, Chromosome]:
            """
            Obtiene a dos individuos de la población actual para que
            sean padres para este método de seleción.

            Returns:
                Una tupla de cromosomas en la pobleción a tratar como
                padres.
            """

            def roulette() -> int:
                total_fitness = sum(
                    individual.fitness_calc()
                    for individual in self.__current_population
                )
                selection_point = random.uniform(0, total_fitness)
                cumulative_fitness = 0
                selected_index = 0
                for i in range(len(self.__current_population)):
                    cumulative_fitness += self.__current_population[i].fitness_calc()
                    if cumulative_fitness >= selection_point:
                        selected_index = len(self.__current_population) - (i+1)
                        break
                return selected_index

            first_index = roulette()
            second_index = roulette()
            while first_index == second_index: second_index = roulette()
            return (
                self.__current_population[first_index],
                self.__current_population[second_index]
            )

        new_population: List[Chromosome] = []
        time_without_feasible = 0
        limit = False
        while len(new_population) < self.__population_size and not limit:
            limit = time_without_feasible >= self.__max_time_without_feasible
            parents = get_parents()
            child = self.__generate_child(parents[0], parents[1])
            time_without_feasible += 1
            if child.feasible and child not in new_population:
                new_population.append(child)
                time_without_feasible -= self._time_constant\
                    if time_without_feasible >= self._time_constant else 0
        return new_population

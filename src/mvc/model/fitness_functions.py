"""Fitness Functions

Módulo que tiene varias funciones que sirven para calcular el valor de
aptitud de una solución.
"""

from typing import List, Tuple

def fitness_distance(
        distance: float, vehicle_info: Tuple[int, int, int],
        service_time: float, route_distance: List[int]
    ) -> float:
    """
    El objetivo de esta función es atender a los clientes empleando la
    menor cantidad de recursos posibles si tomamos en cuenta que la
    distancia total se puede traducir como combustible y la cantidad de
    vehículos empleada como activos que podrían ser utilizados en otras
    actividades.

    Args:
        distance (float): La distancia que se recorrió en total en la
            solución propuesta.
        vehicle_info (Tuple[int, int, int]): Información respecto a los
            vehículos. El primer dato es cuántos vehículos se emplearon
            en la solución; el segundo la cantidad mínima de vehículos
            que se espera usar; y el tercero la cantidad máxima de
            vehículos que se espera usar.
        service_time (float): El tiempo total que los vehículos
            estuvieron de servicio. Además de contar la distancia que
            recorrieron, tambien considera los tiempos muertos donde
            los vehículos tuvieron que esperar.

    Returns:
        El valor númerico para la solución.
    """

    return 0.5 * distance + (vehicle_info[0] - vehicle_info[1]) * vehicle_info[2]

def fitness_vehicle_number(
        distance: float, vehicle_info: Tuple[int, int, int],
        service_time: float, route_distance: List[int]
    ) -> float:
    """
    El objetivo de esta función es distribuír la carga de trabajo de
    los vehículos de una forma más uniforme entre todos.

    Args:
        distance (float): La distancia que se recorrió en total en la
            solución propuesta.
        vehicle_info (Tuple[int, int, int]): Información respecto a los
            vehículos. El primer dato es cuántos vehículos se emplearon
            en la solución; el segundo la cantidad mínima de vehículos
            que se espera usar; y el tercero la cantidad máxima de
            vehículos que se espera usar.
        service_time (float): El tiempo total que los vehículos
            estuvieron de servicio. Además de contar la distancia que
            recorrieron, tambien considera los tiempos muertos donde
            los vehículos tuvieron que esperar.

    Returns:
        El valor númerico para la solución.
    """

    # mean = (distance / vehicle_info[0])
    # sum = 0
    # for distance in route_distance:
    #     sum += abs(distance - mean)
    # return sum
    return vehicle_info[2] * distance - vehicle_info[1] * distance

def fitness_service_time(
        distance: float, vehicle_info: Tuple[int, int, int],
        service_time: float, route_distance: List[int]
    ) -> float:
    """
    El objetivo de esta función es evitar los tiempos muertos donde se
    llega al destino antes de tiempo y el transportista tiene que
    esperar para poder despachar a su cliente y poder continuar con su
    ruta asignada.

    Args:
        distance (float): La distancia que se recorrió en total en la
            solución propuesta.
        vehicle_info (Tuple[int, int, int]): Información respecto a los
            vehículos. El primer dato es cuántos vehículos se emplearon
            en la solución; el segundo la cantidad mínima de vehículos
            que se espera usar; y el tercero la cantidad máxima de
            vehículos que se espera usar.
        service_time (float): El tiempo total que los vehículos
            estuvieron de servicio. Además de contar la distancia que
            recorrieron, tambien considera los tiempos muertos donde
            los vehículos tuvieron que esperar.

    Returns:
        El valor númerico para la solución.
    """

    return distance + (1 / max(vehicle_info[2] - vehicle_info[0], 1)) * service_time

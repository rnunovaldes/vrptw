"""Chronometer

Módulo que tiene una clase útil para contar el tiempo en que se ha
realizado algo
"""

import time
from typing import List

class Chronometer():
    """
    Clase que puede contar cuantos segundos se ha realizado algo

    Attributes:
        __start_time: El momento en que se inicia a tomar el tiempo
        __pause_time: El momento en que se deja de tomar el tiempo
            temporalmente
        __pause_periods: Una lista con los tiempos que se demoraron las
            distintas pausas que pudo tener
        __paused: Si el conteo de tiempo está detenido de momento

    Methods:
        start: Inicia el cronómetro
        pause: Pausa el cronómetro
        stop: Detiene el cronómetro y devuelve el tiempo que se contó
        is_paused: Indica si el cronómetro está pausado
    """

    def __init__(self):
        """
        Inicia un objeto de la clase sin ninguna información
        """

        self.__start_time: float = None
        self.__pause_time: float = None
        self.__pause_periods: List[float] = []
        self.__paused = True

    def start(self):
        """
        Inicia el conteo del tiempo si no se ha iniciado o lo reanuda
        si si pausó con anterioridad
        """

        if self.__pause_time is not None:
            pause_duration = time.time() - self.__pause_time
            self.__pause_periods.append(pause_duration)
            self.__pause_time = None
        else: self.__start_time = time.time()
        self.__paused = False

    def pause(self):
        """
        Pausa el tiempo del cronómetro, registrando el tiempo  en que
        se detiene para luego descontralo del tiempo total
        """

        if self.__start_time is not None and self.__pause_time is None:
            self.__pause_time = time.time()
            self.__paused = True

    def stop(self) -> float:
        """
        Devuelve el tiempo que se registró por el cronómeto y regresa
        el cronómetro a su estado inicial sin información

        Returns:
            El tiempo que se registró desde que se hizo start hasta
            stop
        """

        if self.__pause_time is not None: self.start()
        time_running = time.time() - self.__start_time
        pause_duration = sum(self.__pause_periods)
        self.__start_time = None
        self.__pause_time = None
        self.__pause_periods = []
        return time_running - pause_duration

    def is_paused(self) -> bool:
        """
        Indica si el cronómetro está pausado

        Returns:
            true si está pausado, false en otro caso
        """

        return self.__paused

"""Manager

Módulo que tiene una clase para servir de puente entre la vista y el
modelo del programa y unas listas con información útil para el modelo.
"""

import threading
import pandas as pd

from mvc.controller.observer import Observable, Observer
from mvc.controller.chronometer import Chronometer
from mvc.model.crossover_methods import one_point_crossover, order_crossover
from mvc.model.fitness_functions import *
from mvc.model.model import Model
from mvc.view.window import Window

# Una lista con los dataframe de la información de los clientes
CLIENT_LISTS = {
    0: pd.read_csv('res/C101.csv'),
    1: pd.read_csv('res/C201.csv'),
    2: pd.read_csv('res/R101.csv'),
    3: pd.read_csv('res/R208.csv'),
    4: pd.read_csv('res/SPIRAL.csv'),
    5: pd.read_csv('res/LOTTIME.csv'),
    6: pd.read_csv('res/PENTAGON.csv'),
}
# Identificadores de métodos de selección de padres
SELECTION_METHODS = {
    0: 'tournament',
    1: 'elitism',
    2: 'roulette'
}
# Funciones que pueden generar una lista en base a otras 2
CROSSOVER_METHODS = {
    0: one_point_crossover,
    1: order_crossover
}
# Funciones para dar valor numérico a soluciones
FITNESS_FUNCTION = {
    0: fitness_distance,
    1: fitness_vehicle_number,
    2: fitness_service_time,
}
DEFAULTS = {
    0: {
        'solution': [
            81, 78, 76, 71, 70, 73, 77, 79, 80,
            57, 55, 54, 53, 56, 58, 60, 59,
            98, 96, 95, 94, 92, 93, 97, 100, 99,
            32, 33, 31, 35, 37, 38, 39, 36, 34,
            13, 17, 18, 19, 15, 16, 14, 12,
            90, 87, 86, 83, 82, 84, 85, 88, 89, 91,
            43, 42, 41, 40, 44, 46, 45, 48, 51, 50, 52, 49, 47,
            67, 65, 63, 62, 74, 72, 61, 64, 68, 66, 69,
            5, 3, 7, 8, 10, 11, 9, 6, 4, 2, 1, 75,
            20, 24, 25, 27, 29, 30, 28, 26, 23, 22, 21
        ],
        'breakpoints': [80, 59, 99, 34, 12, 91, 47, 69, 75, 21, float('inf')],
        'capacity': 200,
        'routes': 10
    },
    1: {
        'solution': [
            93, 5, 75, 2, 1, 99, 100, 97, 92, 94, 95, 98, 7, 3, 4, 89, 91, 88, 84, 86, 83, 82, 85, 76, 71, 70, 73, 80, 79, 81, 78, 77, 96, 87, 90,
            67, 63, 62, 74, 72, 61, 64, 66, 69, 68, 65, 49, 55, 54, 53, 56, 58, 60, 59, 57, 40, 44, 46, 45, 51, 50, 52, 47, 43, 42, 41, 48,
            20, 22, 24, 27, 30, 29, 6, 32, 33, 31, 35, 37, 38, 39, 36, 34, 28, 26, 23, 18, 19, 16, 14, 12, 15, 17, 13, 25, 9, 11, 10, 8, 21
        ],
        'breakpoints': None,
        'capacity': 700,
        'routes': 3
    },
    2: {
        'solution': [
            2, 21, 73, 41, 56, 4,
            5, 83, 61, 85, 37, 93,
            14, 44, 38, 43, 13,
            27, 69, 76, 79, 3, 54, 24, 80,
            28, 12, 40, 53, 26,
            30, 51, 9, 66, 1,
            31, 88, 7, 10,
            33, 29, 78, 34, 35, 77,
            36, 47, 19, 8, 46, 17,
            39, 23, 67, 55, 25,
            45, 82, 18, 84, 60, 89,
            52, 6,
            59, 99, 94, 96,
            62, 11, 90, 20, 32, 70,
            63, 64, 49, 48,
            65, 71, 81, 50, 68,
            72, 75, 22, 74, 58,
            92, 42, 15, 87, 57, 97,
            95, 98, 16, 86, 91, 100
        ],
        'breakpoints': None,
        'capacity': 200,
        'routes': 19
    },
    3: {
        'solution': [
            27, 52, 7, 88, 31, 10, 62, 11, 63, 90, 32, 30, 70, 69, 1, 50, 76, 12, 26, 28, 53, 2, 57, 42, 87, 97, 59, 99, 5, 84, 17, 45, 8, 46, 47, 36, 49, 64, 19, 48, 82, 18, 83, 60, 93, 91, 100, 37, 95, 94, 13,
            89, 6, 96, 92, 98, 85, 61, 16, 86, 44, 38, 14, 43, 15, 41, 22, 75, 56, 23, 67, 39, 24, 29, 79, 33, 81, 9, 51, 20, 66, 65, 71, 35, 34, 78, 3, 77, 68, 80, 54, 55, 25, 4, 72, 74, 73, 21, 40, 58
        ],
        'breakpoints': [13, float('inf')],
        'capacity': 1000,
        'routes': 2
    },
    4: {
        'solution': [
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22
        ],
        'breakpoints': None,
        'capacity': 1000,
        'routes': 1
    },
    5: {
        'solution': [

        ],
        'breakpoints': None,
        'capacity': 100,
        'routes': 5
    },
    6: {
        'solution': [

        ],
        'breakpoints': None,
        'capacity': 1000,
        'routes': 5
    },
}

class Manager(Observer, Observable):
    """
    Attributes:
        _is_running: Indica si el AG está en ejecución actualmente.
        _max_iterations: La cantidad de iteraciones que se harán.
        _iterations: Las iteraciones que se llevan hechas.
        _max_non_productive: La cantidad de iteraciones que pueden
            pasar sin que haya mejora en la solución final.
        _last_productive: Las iteraciones que han pasado desde la
            última mejor solución final.
        _best_hash: El identidicados del último mejor resultado final.
        __model: El modelo que tiene lo necesario para crear la lógica
            del AG.

    Methods:
        start: Indica que debe comenzarse el AG y lo repite hasta que
            se cumpla la cantidad deseada de iteraciones.
    """

    _is_running: bool = False
    _max_iterations = float('inf')
    _iterations = 0
    _max_non_productive = 10
    _last_productive = 0
    _best_hash = None
    _chronometer = Chronometer()

    def __init__(self):
        """
        Crea una lógica para la ejecución del algoritmo genético.
        """

        super().__init__()

        window = Window(CLIENT_LISTS)
        self.__model = Model()
        window.add_observer(self)
        self.__model.add_observer(window)
        window.startloop()

    def start(self) -> None:
        """
        Inicia o reanuda el algoritmo genético.
        """

        if not Manager._is_running:
            Manager._is_running = True
        def simulation_thread():
            while Manager._is_running\
                    and Manager._iterations <= Manager._max_iterations:
                new_info = self.__model.do_simulation(
                    Manager._iterations, Manager._last_productive
                )
                if Manager._last_productive == Manager._max_non_productive\
                        or Manager._iterations == Manager._max_iterations\
                        or new_info[1]:
                    self.__model.stop(Manager._chronometer.stop())
                    Manager._iterations = 0
                    Manager._last_productive = 0
                    Manager._best_hash = None
                    Manager._is_running = False
                if Manager._best_hash == new_info[0]:
                    Manager._last_productive += 1
                else:
                    Manager._last_productive = 0
                    Manager._best_hash = new_info[0]
                Manager._iterations += 1

        thread = threading.Thread(target=simulation_thread)
        thread.start()

    def update(self, event: str, data=None) -> None:
        """
        Método que realiza acciones al recibir una notificación.

        Args:
            event (str): Banderas que identifican al evento:
                * "set_parameters" Indica que se deben asignar
                    parámetros para el AG,
                * "start" Indica que se debe iniciar el AG,
                * "stop" Indica que se debe detener el AG,
                * "reset" Indica que se debe reiniciar el AG.
            data (Any): Un dato que es pasado al realizarse la
                notificación:
                * "set_parameters" Tiene los parámetros necesarios para
                    el AG que son
                    (clients_data: DataFrame,
                     max_capacity: int,
                     max_operation_time: float,
                     iterations: int,
                     population_size: int,
                     mutation_rate: float,
                     selection_method: str,
                     crossover: function,
                     distance: int,
                     fitness: function,
                     min_vehicles: int,
                     max_vehicles: int,
                     name: str,
                     better_son: int
                     always_mut: int),
                * "start" No recibe datos,
                * "stop" No recibe datos,
                * "reset" No recibe datos.
        """

        if event == 'set_parameters':
            if Manager._iterations == 0:
                dataframe = CLIENT_LISTS[data[0]]
                time = data[2] if data[2] != 0 else float('inf')
                Manager._max_iterations = data[3] if data[3] != 0 else float('inf')
                Manager._max_non_productive = data[4] if data != 0 else 1
                population_size = data[5] if data[5] > 1 else 2
                selection = SELECTION_METHODS[data[8]]
                crossover = CROSSOVER_METHODS[data[9]]
                fitness = FITNESS_FUNCTION[data[11]]
                self.__model.set_parameters(
                    dataframe, data[1], time,
                    Manager._max_iterations, population_size, data[6], data[7],
                    selection, crossover, data[10], fitness, data[12], data[13],
                    data[-3], (True if data[-2] == 1 else False),
                    (True if data[-1] == 0 else False)
                )
        elif event == 'start':
            Manager._chronometer.start()
            self.start()
        elif event == 'stop':
            Manager._is_running = False
            Manager._chronometer.pause()
        elif event == 'reset':
            if Manager._chronometer.is_paused() and Manager._is_running:
                self.__model.stop(Manager._chronometer.stop())
            Manager._is_running = False
            Manager._iterations = 0
            Manager._last_productive = 0
            Manager._best_hash = None
        elif event == 'default':
            dataframe = CLIENT_LISTS[data[0]]
            default = DEFAULTS[data[0]]
            self.__model.get_default(
                dataframe, default['capacity'], FITNESS_FUNCTION[0],
                default['routes'], default['routes'], default['solution'],
                data[1], default['breakpoints']
            )
        elif event == 'save':
            self.__model.stop(Manager._chronometer.stop())

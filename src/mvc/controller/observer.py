"""Observer

Módulo con una interfaz para observadores y observados.
"""

from typing import Any, List

class Observer:
    """
    Una clase con el propósito de ser implementada por los
    observadores.

    Methods:
        update: Actualiza su información respecto a quien observa.
    """

    def update(self, event: str, data: Any=None) -> None:
        """
        Método que realiza acciones al recibir una notificación

        Args:
            event (str): Banderas que identifican al evento.
            data (Any): Un dato que es pasado al realizarse la
                notificación.
        """

        pass

class Observable:
    """
    Una clase con el propósito de ser implementada por los observados.

    Attributes:
        _observers: Una lista con todos los observadores de la clase.

    Methods:
        add_observer: Agrega un observador a su lista.
        remove_observer: Elimina un observador de su lista.
        notify_observers: Envía información a sus observadores.
    """

    def __init__(self) -> None:
        """
        Inicia una lista vacía de observadores.
        """

        self._observers: List[Observer] = []

    def add_observer(self, observer: Observer) -> None:
        """
        Agrega un observador a su lista.

        Args:
            observer (Observer): La clase que va recibir
                notificaciones.
        """

        self._observers.append(observer)

    def remove_observer(self, observer: Observer) -> None:
        """
        Elimina a un observador de su lista.

        Args:
            observer (Observer): La clase que ya no va a recibir
                notificaciones.
        """

        self._observers.remove(observer)

    def notify_observers(self, event: str, data: Any=None) -> None:
        """
        Notifica a los observadores que ha habido un cambio.

        Args:
            event (str): La forma de identificar el evento.
            data (Any): La información que se envía.
        """

        for observer in self._observers:
            observer.update(event, data)

"""SoundManager

Módulo que tiene una clase para tener una forma auditiva de avisar que
ha ocurrido algo.
"""

FILE = 'res/alarm.mp3'

class SoundManager:
    """
    Attributes:
        __pygame: Un objeto Pygame para reproducir el sonido.
        __sound_enabled: Indica si la librería pygame está disponible.

    Methods:
        play_sound:
        stop_sound:
    """

    __instance = None

    def __new__(cls, *args, **kwargs):
        """
        Método para asegurar que solo haya una instancia de la clase.
        """
        if not cls.__instance:
            cls.__instance = super(SoundManager, cls).__new__(cls)
        return cls.__instance

    def __init__(self):
        """
        Crea un objeto que puede reproducir sonido.
        """

        if not hasattr(self, 'initialized'):  # Asegura que solo se inicializa una vez
            try:
                import pygame
                self.__pygame = pygame
                self.__pygame.mixer.init()
                self.__sound_enabled = True
            except ImportError:
                print('Pygame no está instalado. El sonido no estará disponible.')
                self.__pygame = None
                self.__sound_enabled = False
            self.initialized = True

    def play_sound(self) -> None:
        """
        Reproduce el sonido de ser posible.
        """

        if not self.__sound_enabled:
            print('Función de sonido no disponible.')
            return

        try:
            self.__pygame.mixer.music.load(FILE)
            self.__pygame.mixer.music.play(loops=-1)
        except Exception as e:
            print(f'Error reproduciendo sonido: {e}')

    def stop_sound(self) -> None:
        """
        Detiene el sonido si este se reproduce
        """

        if self.__sound_enabled and self.__pygame.mixer.music.get_busy():
            self.__pygame.mixer.music.stop()

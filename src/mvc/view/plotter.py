"""Plotter

Módulo que tiene una clase para crear un lienzo sobre el cual se puede
dibujar puntos y líneas que los unen de distintos colores entre ellas.
"""

import tkinter as tk
from PIL import Image, ImageDraw, ImageFont
from typing import List, Tuple

import colorsys

def generate_rainbow_palette(num_colors) -> List[str]:
    """
    Genera una paleta de colores 'rainbow'.

    Args:
        num_colors: Número de colores en la paleta.

    Returns:
        Una lista de colores en formato hexadecimal.
    """
    colors = []
    for i in range(num_colors):
        hue = i / float(num_colors)
        rgb = colorsys.hsv_to_rgb(hue, 1.0, 1.0)
        hex_color = "#{:02x}{:02x}{:02x}".format(
            int(rgb[0] * 255), int(rgb[1] * 255), int(rgb[2] * 255)
        )
        colors.append(hex_color)
    return colors

class Plotter:
    """
    Clase que sirve para dibujar en un lienzo puntos y líneas
    uniéndolos que representan las rutas a seguir por los vehículos.

    Attributes:
        __canvas: Un liezo para dibujar puntos y líneas.
        __coordinates: Una lista con las coordenadas en el plano
            cartesiano de varios puntos.
        __routes: Una lista de listas de índices de los puntos en
            coordinates que están unidos por líneas.
        __image: una imágen para poder dibujar sobre ella y obtener un
            jpg cuando se solicite con save_as_jpg.

    Methods:
        reset: Reinicia al lienzo.
        set_coordinates: Establece las coordenadas de los puntos a
            dibujar.
        __plot_coordinates: Dibuja los puntos.
        set_lines: Permite establecer las líneas que se dibujaran
            entre los puntos.
        __plot_lines: Dibuja las líneas correspondientes entre cada
            punto.
        redraw: Función para repintar los puntos y las líneas
            manteniendo el aspecto del tamaño del lienzo.
        scale_x: Función para escalar la coordenada x al ancho del
            lienzo.
        scale_y: Función para escalar la coordenada y a la altura del
            lienzo.
        save_as_jpg: Permite guardar el canvas como una imágen.
    """

    def __init__(self, frame: tk.Frame):
        """
        Constructor de un objeto que puede dibujar en pantalla puntos y
        líneas.

        Parameters:
            frame (tk.Frame): El cuadro padre que contendrá al canvas
                de esta clase.
        """

        self.__canvas = tk.Canvas(frame, bg='white')
        self.__canvas.pack(expand=tk.YES, fill=tk.BOTH)

    def reset(self) -> None:
        """
        Reinicia al lienzo para que pueda dibujarse nuevos puntos y
        nuevas líneas.
        """

        self.__canvas.delete(tk.ALL)
        self.__image = Image.new('RGB', (
            self.__canvas.winfo_width(), self.__canvas.winfo_height()
        ), 'white')
        if hasattr(self, '__coordinates'): delattr(self, '__coordinates')
        if hasattr(self, '__routes'): delattr(self, '__routes')

    def set_coordinates(
            self, coordinates: List[Tuple[float, float]]
        ) -> None:
        """
        Establece las coordenadas de los puntos y mantiene el orden en
        el que vienen. Dibuja los puntos después.

        Args:
            coordenates (List[Tuple[float, float]]): Una lista de
                tuplas con las coordenadas x e y para cada punto.
                El orden que tenga está lista es de relevancia para
                asignarles un identificador a cada punto y comienza
                desde el 0.
        """

        self.__canvas.delete(tk.ALL)
        self.__image = Image.new('RGB', (
            self.__canvas.winfo_width(), self.__canvas.winfo_height()
        ), 'white')
        draw = ImageDraw.Draw(self.__image)
        self.__coordinates = coordinates
        self.__plot_coordinates(draw)

    def set_lines(self, routes: List[List[int]]) -> None:
        """
        Permite establecer las líneas que se dibujaran entre los puntos
        para que sean dibujados posteriormente y vuelve a dibujar los
        puntos

        Args:
            routes (List[List[int]]): Los índices de los puntos
                separados en varias listas, donde los índices indican
                el orden en que se unen los puntos y si están en listas
                distintas entonces no hay línea entre ninguno de los
                puntos de las listas disjuntas.
        """

        self.__canvas.delete(tk.ALL)
        self.__image = Image.new('RGB', (
            self.__canvas.winfo_width(), self.__canvas.winfo_height()
        ), 'white')
        draw = ImageDraw.Draw(self.__image)
        self.__routes = routes
        self.__plot_lines(draw)
        self.__plot_coordinates(draw)

    def __plot_coordinates(self, draw: ImageDraw) -> None:
        """
        Dibuja los puntos correspondientes.

        Args:
            draw (ImageDraw): Un elemento que sirve para poder dibujar
                sobre él y que se pueda objener una imagen JPG cuando
                se solicite con save_as_jpg.
        """

        for i, (x, y) in enumerate(self.__coordinates):
            canvas_x = self.scale_x(x)
            canvas_y = self.scale_y(y)
            self.__canvas.create_oval(
                canvas_x - 3, canvas_y - 3, canvas_x + 3, canvas_y + 3,
                fill='blue'
            )
            draw.ellipse(
                (canvas_x - 3, canvas_y - 3, canvas_x + 3, canvas_y + 3),
                fill='blue'
            )
            label_x = canvas_x + 5
            label_y = canvas_y - 5
            self.__canvas.create_text(
                label_x, label_y, text=str(i), anchor=tk.W
            )
            font = ImageFont.load_default()  # Selecciona una fuente por defecto
            draw.text((label_x, label_y), str(i), fill='black', font=font)

    def __plot_lines(self, draw: ImageDraw) -> None:
        """
        Dibuja las líneas correspondientes entre cada punto.

        Args:
            draw (ImageDraw): Un elemento que sirve para poder dibujar
                sobre él y que se pueda objener una imagen JPG cuando
                se solicite con save_as_jpg.
        """

        colors = generate_rainbow_palette(len(self.__routes))
        for route, color in zip(self.__routes, colors):
            for i in range(len(route)):
                client = route[i]
                if i == 0:
                    x1, y1 = self.__coordinates[0]
                    x2, y2 = self.__coordinates[client]
                    canvas_x1, canvas_y1 = self.scale_x(x1), self.scale_y(y1)
                    canvas_x2, canvas_y2 = self.scale_x(x2), self.scale_y(y2)
                    self.__canvas.create_line(
                        canvas_x1, canvas_y1, canvas_x2, canvas_y2, fill=color
                    )
                    draw.line(
                        (canvas_x1, canvas_y1, canvas_x2, canvas_y2),
                        fill=color
                    )
                x1, y1 = self.__coordinates[client]
                x2, y2 = self.__coordinates[0]
                if i < len(route)-1: x2, y2 = self.__coordinates[route[i+1]]
                canvas_x1, canvas_y1 = self.scale_x(x1), self.scale_y(y1)
                canvas_x2, canvas_y2 = self.scale_x(x2), self.scale_y(y2)
                self.__canvas.create_line(
                    canvas_x1, canvas_y1, canvas_x2, canvas_y2, fill=color
                )
                draw.line(
                    (canvas_x1, canvas_y1, canvas_x2, canvas_y2), fill=color
                )

    def redraw(self, event) -> None:
        """
        Función que debe ser invocada cada que se tenga que repintar el
        lienzo (como cuando hay un cambio en el tamaño de la ventana).
        """

        self.__canvas.delete(tk.ALL)
        self.__image = Image.new('RGB', (
            self.__canvas.winfo_width(), self.__canvas.winfo_height()
        ), 'white')
        draw = ImageDraw.Draw(self.__image)
        if hasattr(self, '__coordinates'): self.__plot_coordinates(draw)
        if hasattr(self, '__routes'): self.__plot_lines(draw)

    def scale_x(self, x: int) -> float:
        """
        Función para escalar la coordenada x al ancho del lienzo.

        Args:
            x: El valor a escalar en el eje de las abscisas.

        Returns:
            El valor de x escalado según el tamaño del lienzo.
        """

        return x * (
            self.__canvas.winfo_width() / (max(x for x, y in self.__coordinates) + 5)
        ) + 10

    def scale_y(self, y: int) -> float:
        """
        Función para escalar la coordenada y a la altura del lienzo.

        Args:
            y: El valor a escalar en el eje de las ordenadas.

        Returns:
            El valor de y escalado según el tamaño del lienzo.
        """

        return y * (
            self.__canvas.winfo_height() / (max(y for x, y in self.__coordinates) + 5)
        ) + 10

    def save_as_jpg(self, filename: str) -> None:
        """
        Guarda el contenido del lienzo como un archivo JPG.
        Lo pone en la carpeta log y le pone extensión.

        Args:
            filename (str): Nombre del archivo JPG de salida.
        """

        self.__image.save('log/' + filename + '.jpg')

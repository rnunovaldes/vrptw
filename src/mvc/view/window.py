"""Window

Módulo con la clase que sirve como vista y notifica al controlador de
cambios en esta.
"""

import tkinter as tk
from tkinter import messagebox, ttk
from typing import Any, List

from pandas import DataFrame

from mvc.controller.observer import Observer, Observable
from mvc.controller.sound_manager import SoundManager
from mvc.view.plotter import Plotter

# Título que tendrá la ventana
TITLE = 'Algoritmo genético para VRPTW'
# Una lista con los nombres de las secciones de la ventana
LABELS = [
    'Lista de Clientes', #0
    'Cantidad Máxima de Productos', #1
    'Tiempo máximo de operación', #2
    'Iteraciones', #3
    'Tiempo máximo sin mejora', #4
    '# Individuos x Generación', #5
    'Tiempo máximo sin generar Individuo', #6
    'Probabilidad de mutación', #7
    'Tipo de selección', #8
    'Tipo de cruzamiento', #9
    'ITERACIÓN #{:d}\tTIEMPO SIN MEJORA #{:d}', #10
    ' FINALIZADO', #11
    'Distancia para que los clientes sean cercanos', #12
    'Función de aptitud', #13
    'Mínimo de vehículos', #14
    'Máximo de vehículos', #15
]
# Opciones de la barra superior
SIDEBAR = [
    'Algoritmo', #0
    'Iniciar', #1
    'Detener', #2
    'Reiniciar', #3
    'Ayuda', #4
    'Programa', #5
    'Vehículo', #6
    'AG', #7
    'Salir', #8
    'Mejor Solución'
]
# Opciones que hay para la lista de clientes
CLIENTS_OPTIONS = [
    'C101', #0
    'C201', #1
    'R101', #2
    'R208', #3
    'Espiral',
    'Mucho',
    'Pentágono'
]
# Opciones que hay para el tipo de selección de padres
SELECTION_OPTIONS = [
    'Torneo Binario', #0
    'Elitismo', #1
    'Ruleta' #2
]
# Opciones que hay para el tipo de cruzamiento
CROSSOVER_OPTIONS = [
    'Cruce de un punto', #0
    'Cruce ordenado' #1
]
# Opciones que hay para la función de aptitud
FITNESS_OPTIONS = [
    'Preferencia a distancia', #0
    'Distribución de trabajo uniforme', #1
    'Eliminación de tiempos muertos' # 2
]
ELITISM_OPTIONS = [
    'Elitismo en Hijos', #0
    'Siempre aleatorio', #1
    'Siempre el mejor', #2
    'Elitismo en Mutación', #3
    'Siempre mutar', #4
    'Solo con mejora', #5
]
# Una lista con un mensajes para ventanas emergentes
MESSAGES = {
    0: {
        'title': 'Ayuda (Programa)',
        'message': f'En "{SIDEBAR[0]}" están las opciones para {SIDEBAR[1]}, \
{SIDEBAR[2]} o {SIDEBAR[3]} la ejecución del AG.\n\n\
En "{SIDEBAR[1]}" se comienza la ejecución del AG con los parámetros dados.\n\
En "{SIDEBAR[2]}" se detiene la ejecución actual del AG y la guarda en vez \
de reiniciarla.\n\
En "{SIDEBAR[3]}" se olvida la ejecución detenida anteriormente para poder \
comenzar una nueva.'
    },
    1: {
        'title': 'Ayuda (Parámetros del Vehículo)',
        'message': f'En "{LABELS[0]}" se puede escoger la lista de clientes \
que se quiere usar.\n\n\
En "{LABELS[1]}" se escribe la cantidad de productos que puede llevar el \
vehículo por viaje. Puede ser un número entero desde 0 (aunque este no \
tendría solución) hasta 9999.\n\n\
En "{LABELS[2]}" se escribe la cantidad de tiempo que dispone el vehículo por \
ruta. Puede ser un número decimal desde 0 (interpretado como "infinito") \
hasta 9999.'
    },
    2: {
        'title': 'Ayuda (Parámetros del AG)',
        'message': f'En "{LABELS[3]}" se escribe la cantidad de iteraciones \
del AG. Puede ser un número entero de 0 (interpretado como "infinito") hasta \
9999.\n\n\
En "{LABELS[4]}" se escribe la cantidad de iteraciones del AG que pueden \
pasar sin que haya una sola mejora en el resultado final y si no hay mejora \
pasado el tiempo, termina. Puede ser un número entero de 0 (interpretado como \
1) hasta 9999.\n\n\
En "{LABELS[5]}" se escribe la cantidad de individuos que tendrá cada \
generación. Puede ser un número entero desde 0 (interpretando 0 y 1 como 2) \
hasta 9999.\n\n\
En "{LABELS[6]}" se escribe la cantidad de iteraciones del AG que pueden \
pasar sin que se produzca un hijo nuevo y si no lo hay deja de tratar de \
crear hijos para esa generación y pasa a la siguiente. Puede ser un número \
entero de 0 hasta 9999.\n\n\
En "{LABELS[7]}" se escribe la probabilidad de que un individuo mute en el \
AG. Puede ser un número decimal desde 0 hasta 100.\n\n\
En "{LABELS[8]}" se puede escoger el tipo de selección de padres que se desea \
usar.\n\n\
En "{LABELS[9]}" se puede seleccionar el tipo de cruzamiento que se usará \
para crear descendencia deados dos padres.'
    }
}

FILE_NAME: str = '{:s}_{:s}p-{:s}mr-{:s}_sel-{:s}_cros-{:s}_fit-Hijo({:s})-Mut({:s})'
DEFAULT_NAME: str = '{:s}_default'

class Window(Observer, Observable):
    """
    Clase que crea una ventana y todos los elementos visuales para la
    ejecución del algoritmo.

    Attributes:
        __is_paused: Indica si la ejecución está pausada o detenida.
        __clear_text_next: Si debe eliminarse el contenido del
            resultado anterior.
        __window: La ventana principal de la aplicación.
        __file_menu_1: Espacio en la barra superior para iniciar,
            detener y reiniciar la ejecución del AG.
        __clients_var: Tiene la opción seleccionada para la lista de
            clientes que se quiere resolver el problema.
        __max_capacity_var: Tiene la cantidad máxima de productos que
            el vehículo puede llevar por viaje.
        __max_time_var: Tiene el tiempo máximo de operación del
            vehículo para cada ruta.
        __iterations_var: Tiene cantidad de iteraciones del algoritmo.
        __population_size_var: Tiene la cantidad de individuos por
            generación.
        __mutation_rate_var: Tiene la probabilidad de mutación de la
            descendencia.
        __selection_var: Tiene el tipo de selección de padres a usar
        __crossover_var: Tiene el tipo de cruzamiento a usar.
        __result_label: Espacio donde se escriben los resultados de
            cada iteración del AG.
        __plotter: Un liezo para dibujar una representación de los
            clientes y las rutas que toman los vehículos para
            satisfacer sus demandas.

    Methods:
        on_validate_int: Valida cadenas como números enteros entre 0 y
            9999.
        on_validate_float: Valida cadenas como números decimales entre
            0 y 9999.
        on_validate_percentage: Valida cadenas como números decimales
            entre 0 y 100.
        on_spinbox_change: Usado para que el espacio donde se escribe
            de un spinbox solo se admitan números.
        startloop: Inicia la ventana para que sea visible.
        show_message: Hace una ventana emergente con un mensaje
            predefinido en MESSAGES.
        __toggle_simulation_state: Activa o desactiva las opciones para
            ejecutar el AG dependiendo de si está pausado o no en el
            momento.
        __start_simulation: Comienza la ejecución del AG con los
            parámetros dados.
        __stop_simulation: Detiene la ejecución del AG temporalmente.
        __restart_simulation: Reanuda la ejecución del AG si hay una
            pendiente.
        __quit: Detiene la ejecución del AG de ser necesario y termina
            el programa.
    """

    __is_paused = True
    __clear_on_next = False
    __spinbox_elements: List[ttk.Spinbox] = []
    __radiobutton_elements: List[ttk.Radiobutton] = []
    __manual_stopped = False

    sound = SoundManager()

    @staticmethod
    def on_validate_int(P: str) -> bool:
        """
        Valida una cadena como un número entero entre 0 y 9999.

        Args:
            P (str): Una cadena para ser probada.
        """

        if P == '' or (P.isdigit() and 0 <= int(P) <= 9999): return True
        else: return False

    @staticmethod
    def on_validate_float(P: str) -> bool:
        """
        Valida una cadena como un número decimal en un rango dado.

        Args:
            P (str): Una cadena para ser probada.
        """

        if P == '': return True
        try:
            float_value = float(P)
            return 0 <= float_value <= 9999
        except ValueError:
            return False

    @staticmethod
    def on_validate_percentage(P: str) -> bool:
        """
        Valida una cadena como un número decimal en un rango dado.

        Args:
            P (str): Una cadena para ser probada.
        """

        if P == '': return True
        try:
            float_value = float(P)
            return 0 <= float_value <= 100
        except ValueError:
            return False

    @staticmethod
    def on_spinbox_change(event: tk.Event) -> None:
        """
        Da la función de que siempre haya un número en el spinbox.

        Args:
            event (tk.Event): Un evento que tiene al spinbox que lo
            activó.
        """

        spiner: ttk.Spinbox = event.widget
        value: str = spiner.get()
        if value == '': spiner.set('0')
        elif value[0] == '0' and len(value) != 1:
            if float(value) < 1: spiner.set(float(value))
            else: spiner.set(value[1:])

    @staticmethod
    def show_message(type: int) -> None:
        """
        Muestra un mensaje como una ventana emergente.

        Args:
            type (int): Un identificador para el mensaje en una lista
                predefinida.
        """

        message_info = MESSAGES[type]
        messagebox.showinfo(message_info['title'], message_info['message'])

    def __init__(self, client_lists: DataFrame) -> None:
        """
        Constructor que crea una ventana con varias opciones editables
        para realizar una ejecución del algoritmo genético.

        Args:
            client_lists (DataFrame): Listas de todas las listas de los
                clientes para generar una representación gráfica de
                estos.
        """

        super().__init__()

        self.CLIENT_LISTS = client_lists
        self.__window: tk.Tk = tk.Tk()
        SCREEN_WIDTH: int = self.__window.winfo_screenwidth()
        SCREEN_HEIGHT: int = self.__window.winfo_screenheight()
        WINDOW_WIDTH: int = SCREEN_WIDTH-100 if SCREEN_WIDTH > 749 else 650
        WINDOW_HEIGHT: int = SCREEN_HEIGHT-130 if SCREEN_HEIGHT > 629 else 550
        CENTER_X: int = int((SCREEN_WIDTH - WINDOW_WIDTH) / 2)
        self.__window.geometry(f'{WINDOW_WIDTH}x{WINDOW_HEIGHT}+{CENTER_X}+0')
        self.__window.title(TITLE)
        validate_int = self.__window.register(self.on_validate_int)
        validate_float = self.__window.register(self.on_validate_float)
        validate_percentage = self.__window.register(
            self.on_validate_percentage
        )

        menu_bar: tk.Menu = tk.Menu(self.__window)
        # Algoritmo
        self.__file_menu_1: tk.Menu = tk.Menu(menu_bar, tearoff=0)
        self.__file_menu_1.add_command(
            label=SIDEBAR[1], command=self.__start_simulation
        )
        self.__file_menu_1.add_command(
            label=SIDEBAR[2], state='disabled',
            command=self.__stop_simulation
        )
        self.__file_menu_1.add_separator()
        self.__file_menu_1.add_command(
            label=SIDEBAR[9], command=self.__default_simulation
        )
        self.__file_menu_1.add_command(
            label=SIDEBAR[3], state='disabled',
            command=self.__restart_simulation
        )
        menu_bar.add_cascade(
            label=SIDEBAR[0], menu=self.__file_menu_1
        )
        # Lista de clientes
        self.__file_menu_clients = tk.Menu(menu_bar, tearoff=0)
        menu_bar.add_cascade(label=LABELS[0], menu=self.__file_menu_clients)
        # Elitismo en hijos
        self.__file_menu_sons = tk.Menu(menu_bar, tearoff=0)
        self.__sons_var = tk.IntVar()
        self.__sons_var.set(1)  # Valor por defecto
        for i, option in enumerate(ELITISM_OPTIONS[1:3]):
            self.__file_menu_sons.add_radiobutton(
                label=option,
                variable=self.__sons_var,
                value=i,
            )
        menu_bar.add_cascade(label=ELITISM_OPTIONS[0], menu=self.__file_menu_sons)
        # Elitismo en mutacion
        self.__file_menu_muts = tk.Menu(menu_bar, tearoff=0)
        self.__muts_var = tk.IntVar()
        self.__muts_var.set(1)  # Valor por defecto
        for i, option in enumerate(ELITISM_OPTIONS[4:6]):
            self.__file_menu_muts.add_radiobutton(
                label=option,
                variable=self.__muts_var,
                value=i,
            )
        menu_bar.add_cascade(label=ELITISM_OPTIONS[3], menu=self.__file_menu_muts)

        #Ayuda
        file_menu_2: tk.Menu = tk.Menu(menu_bar, tearoff=0)
        file_menu_2.add_command(
            label=SIDEBAR[5], command=lambda: self.show_message(0)
        )
        file_menu_2.add_command(
            label=SIDEBAR[6], command=lambda: self.show_message(1)
        )
        file_menu_2.add_command(
            label=SIDEBAR[7], command=lambda: self.show_message(2)
        )
        menu_bar.add_cascade(label=SIDEBAR[4], menu=file_menu_2)
        # Salir
        menu_bar.add_command(label=SIDEBAR[8], command=self.__quit)
        self.__window.config(menu=menu_bar)


        options_frame = tk.Frame(self.__window)
        def default_options():
            client = self.__clients_var.get()
            if client == 0:
                self.__max_capacity_var.set('200')
                self.__min_vehicles_var.set('10')
                self.__max_vehicles_var.set('30')
                self.__max_distance_var.set('6')
                self.__max_time_var.set('0')
            elif client == 1:
                self.__max_capacity_var.set('700')
                self.__min_vehicles_var.set('2')
                self.__max_vehicles_var.set('30')
                self.__max_distance_var.set('6')
                self.__max_time_var.set('0')
            elif client == 2:
                self.__max_capacity_var.set('200')
                self.__min_vehicles_var.set('18')
                self.__max_vehicles_var.set('30')
                self.__max_distance_var.set('7')
                self.__max_time_var.set('0')
            elif client == 3:
                self.__max_capacity_var.set('1000')
                self.__min_vehicles_var.set('2')
                self.__max_vehicles_var.set('8')
                self.__max_distance_var.set('7')
                self.__max_time_var.set('0')
            elif client == 4:
                self.__max_capacity_var.set('1000')
                self.__min_vehicles_var.set('1')
                self.__max_vehicles_var.set('4')
                self.__max_distance_var.set('10')
                self.__max_time_var.set('0')
            elif client == 5:
                self.__max_capacity_var.set('1000')
                self.__min_vehicles_var.set('5')
                self.__max_vehicles_var.set('8')
                self.__max_distance_var.set('20')
                self.__max_time_var.set('1100')
            else:
                self.__max_capacity_var.set('1000')
                self.__min_vehicles_var.set('5')
                self.__max_vehicles_var.set('10')
                self.__max_distance_var.set('10')
                self.__max_time_var.set('180')

        self.__clients_var = tk.IntVar()
        self.__clients_var.set(0)  # Valor por defecto
        # Añadir las opciones de clientes al menú
        for i, option in enumerate(CLIENTS_OPTIONS):
            self.__file_menu_clients.add_radiobutton(
                label=option,
                variable=self.__clients_var,
                value=i,
                command=default_options
            )

        vehicles_dual_frame = tk.Frame(options_frame)

        # Cantidad de iteraciones sin mejora final
        min_vehicles_frame = tk.Frame(vehicles_dual_frame, bd=2, relief=tk.GROOVE)
        min_vehicles_frame.pack(side='left', fill='x')
        min_vehicles_label = tk.Label(min_vehicles_frame, text=LABELS[14], wraplength=130)
        min_vehicles_label.pack()
        self.__min_vehicles_var = tk.StringVar()
        min_vehicles_spinbox = ttk.Spinbox(
            min_vehicles_frame, textvariable=self.__min_vehicles_var,
            from_=0, to=9999, width=10,
            validate='key', validatecommand=(validate_int, '%P')
        )
        self.__spinbox_elements.append(min_vehicles_spinbox)
        min_vehicles_spinbox.bind('<KeyRelease>', self.on_spinbox_change)
        self.__min_vehicles_var.set('10')
        min_vehicles_spinbox.pack()

        # Cantidad de iteraciones sin crear un hijo
        max_vehicles_frame = tk.Frame(vehicles_dual_frame, bd=2, relief=tk.GROOVE)
        max_vehicles_frame.pack(side='right', fill='x')
        max_vehicles_label = tk.Label(max_vehicles_frame, text=LABELS[15], wraplength=130)
        max_vehicles_label.pack()
        self.__max_vehicles_var = tk.StringVar()
        max_vehicles_spinbox = ttk.Spinbox(
            max_vehicles_frame, textvariable=self.__max_vehicles_var,
            from_=0, to=9999, width=10,
            validate='key', validatecommand=(validate_int, '%P')
        )
        self.__spinbox_elements.append(max_vehicles_spinbox)
        max_vehicles_spinbox.bind('<KeyRelease>', self.on_spinbox_change)
        self.__max_vehicles_var.set('30')
        max_vehicles_spinbox.pack()

        vehicles_dual_frame.pack()

        # Cantidad de máxima de productos
        max_capacity_frame = tk.Frame(options_frame, bd=2, relief=tk.GROOVE)
        max_capacity_frame.pack(fill='x')
        max_capacity_label = tk.Label(max_capacity_frame, text=LABELS[1])
        max_capacity_label.pack()
        self.__max_capacity_var = tk.StringVar()
        max_capacity_spinbox = ttk.Spinbox(
            max_capacity_frame, textvariable=self.__max_capacity_var,
            from_=0, to=9999,
            validate='key', validatecommand=(validate_int, '%P')
        )
        self.__spinbox_elements.append(max_capacity_spinbox)
        max_capacity_spinbox.bind('<KeyRelease>', self.on_spinbox_change)
        self.__max_capacity_var.set('200')
        max_capacity_spinbox.pack()

        # Tiempo máximo de operación
        max_time_frame = tk.Frame(options_frame, bd=2, relief=tk.GROOVE)
        max_time_frame.pack(fill='x')
        max_time_label = tk.Label(max_time_frame, text=LABELS[2])
        max_time_label.pack()
        self.__max_time_var = tk.StringVar()
        max_time_spinbox = ttk.Spinbox(
            max_time_frame, textvariable=self.__max_time_var,
            from_=0, to=9999,
            validate='key', validatecommand=(validate_float, '%P')
        )
        self.__spinbox_elements.append(max_time_spinbox)
        max_time_spinbox.bind('<KeyRelease>', self.on_spinbox_change)
        self.__max_time_var.set('0')
        max_time_spinbox.pack()

        generations_dual_frame = tk.Frame(options_frame)

        # Cantidad de iteraciones
        iterations_frame = tk.Frame(generations_dual_frame, bd=2, relief=tk.GROOVE)
        iterations_frame.pack(side='left', fill='x')
        iterations_label = tk.Label(iterations_frame, text=LABELS[3])
        iterations_label.pack()
        self.__iterations_var = tk.StringVar()
        iterations_spinbox = ttk.Spinbox(
            iterations_frame, textvariable=self.__iterations_var,
            from_=0, to=9999, width=10,
            validate='key', validatecommand=(validate_int, '%P')
        )
        self.__spinbox_elements.append(iterations_spinbox)
        iterations_spinbox.bind('<KeyRelease>', self.on_spinbox_change)
        self.__iterations_var.set('1000')
        iterations_spinbox.pack()

        # Cantidad de individuos en la población
        population_size_frame = tk.Frame(generations_dual_frame, bd=2, relief=tk.GROOVE)
        population_size_frame.pack(side='right', fill='x')
        population_size_label = tk.Label(population_size_frame, text=LABELS[5])
        population_size_label.pack()
        self.__population_size_var = tk.StringVar()
        population_size_spinbox = ttk.Spinbox(
            population_size_frame, textvariable=self.__population_size_var,
            from_=0, to=9999, width=10,
            validate='key', validatecommand=(validate_int, '%P')
        )
        self.__spinbox_elements.append(population_size_spinbox)
        population_size_spinbox.bind('<KeyRelease>', self.on_spinbox_change)
        self.__population_size_var.set('100')
        population_size_spinbox.pack()

        generations_dual_frame.pack()

        iterations_dual_frame = tk.Frame(options_frame)

        # Cantidad de iteraciones sin mejora final
        non_productive_frame = tk.Frame(iterations_dual_frame, bd=2, relief=tk.GROOVE)
        non_productive_frame.pack(side='left', fill='x')
        non_productive_label = tk.Label(non_productive_frame, text=LABELS[4], wraplength=130)
        non_productive_label.pack()
        self.__non_productive_var = tk.StringVar()
        non_productive_spinbox = ttk.Spinbox(
            non_productive_frame, textvariable=self.__non_productive_var,
            from_=0, to=9999, width=10,
            validate='key', validatecommand=(validate_int, '%P')
        )
        self.__spinbox_elements.append(non_productive_spinbox)
        non_productive_spinbox.bind('<KeyRelease>', self.on_spinbox_change)
        self.__non_productive_var.set('200')
        non_productive_spinbox.pack()

        # Cantidad de iteraciones sin crear un hijo
        no_child_production_frame = tk.Frame(iterations_dual_frame, bd=2, relief=tk.GROOVE)
        no_child_production_frame.pack(side='right', fill='x')
        no_child_production_label = tk.Label(no_child_production_frame, text=LABELS[6], wraplength=130)
        no_child_production_label.pack()
        self.__no_child_production_var = tk.StringVar()
        no_child_production_spinbox = ttk.Spinbox(
            no_child_production_frame, textvariable=self.__no_child_production_var,
            from_=0, to=9999, width=10,
            validate='key', validatecommand=(validate_int, '%P')
        )
        self.__spinbox_elements.append(no_child_production_spinbox)
        no_child_production_spinbox.bind('<KeyRelease>', self.on_spinbox_change)
        self.__no_child_production_var.set('100')
        no_child_production_spinbox.pack()

        iterations_dual_frame.pack()

        # Probabilidad de mutación
        mutation_rate_frame = tk.Frame(options_frame, bd=2, relief=tk.GROOVE)
        mutation_rate_frame.pack(fill='x')
        mutation_rate_label = tk.Label(mutation_rate_frame, text=LABELS[7])
        mutation_rate_label.pack()
        self.__mutation_rate_var = tk.StringVar()
        mutation_rate_spinbox = ttk.Spinbox(
            mutation_rate_frame, textvariable=self.__mutation_rate_var,
            from_=0, to=100,
            validate='key', validatecommand=(validate_percentage, '%P')
        )
        self.__spinbox_elements.append(mutation_rate_spinbox)
        mutation_rate_spinbox.bind('<KeyRelease>', self.on_spinbox_change)
        self.__mutation_rate_var.set('20')
        mutation_rate_spinbox.pack()

        # Tipo de selección de padres
        selection_frame = tk.Frame(options_frame, bd=2, relief=tk.GROOVE)
        selection_frame.pack(fill='x')
        selection_label = ttk.Label(selection_frame, text=LABELS[8])
        selection_label.pack()
        self.__selection_var = tk.IntVar()
        for i in range(len(SELECTION_OPTIONS)):
            selection_option = ttk.Radiobutton(
                selection_frame, text=SELECTION_OPTIONS[i],
                variable=self.__selection_var, value=i
            )
            self.__radiobutton_elements.append(selection_option)
            selection_option.pack()
        self.__selection_var.set(0)

        # Tipo de cruzamiento
        crossover_frame = tk.Frame(options_frame, bd=2, relief=tk.GROOVE)
        crossover_frame.pack(fill='x')
        crossover_label = ttk.Label(crossover_frame, text=LABELS[9])
        crossover_label.pack()
        self.__crossover_var = tk.IntVar()
        for i in range(len(CROSSOVER_OPTIONS)):
            crossover_option = ttk.Radiobutton(
                crossover_frame, text=CROSSOVER_OPTIONS[i],
                variable=self.__crossover_var, value=i
            )
            self.__radiobutton_elements.append(crossover_option)
            crossover_option.pack()
        self.__crossover_var.set(1)
        options_frame.pack(expand=0, side='left', fill='y')

        # Tipo de cruzamiento
        fitness_frame = tk.Frame(options_frame, bd=2, relief=tk.GROOVE)
        fitness_frame.pack(fill='x')
        fitness_label = ttk.Label(fitness_frame, text=LABELS[13])
        fitness_label.pack()
        self.__fitness_var = tk.IntVar()
        for i in range(len(FITNESS_OPTIONS)):
            fitness_option = ttk.Radiobutton(
                fitness_frame, text=FITNESS_OPTIONS[i],
                variable=self.__fitness_var, value=i
            )
            self.__radiobutton_elements.append(fitness_option)
            fitness_option.pack()
        self.__fitness_var.set(1)
        options_frame.pack(expand=0, side='left', fill='y')

        # Distancia para considerar cercanos los cliente
        max_distance_frame = tk.Frame(options_frame, bd=2, relief=tk.GROOVE)
        max_distance_frame.pack(fill='x')
        max_distance_label = tk.Label(max_distance_frame, text=LABELS[12])
        max_distance_label.pack()
        self.__max_distance_var = tk.StringVar()
        max_distance_spinbox = ttk.Spinbox(
            max_distance_frame, textvariable=self.__max_distance_var,
            from_=0, to=9999,
            validate='key', validatecommand=(validate_int, '%P')
        )
        self.__spinbox_elements.append(max_distance_spinbox)
        max_distance_spinbox.bind('<KeyRelease>', self.on_spinbox_change)
        self.__max_distance_var.set('6')
        max_distance_spinbox.pack()

        result_frame = tk.Frame(self.__window, bd=2, relief=tk.GROOVE)
        self.__plotter = Plotter(result_frame)

        self.__result_label = ttk.Label(
            result_frame, text=LABELS[10].format(0, 0)
        )
        self.__result_label.pack(side='left', fill='y', expand=True)
        result_frame.pack(fill='both', expand=True)
        #self.__window.bind("<Configure>", self.__plotter.redraw)

    def startloop(self) -> None:
        """
        Inicia la ventana para que sea visible.
        """

        self.__window.mainloop()

    def __toggle_simulation_state(self) -> None:
        """
        Enciende y apaga opciones según sean necesarias o no.
        """

        if not self.__is_paused:
            self.__file_menu_1.entryconfig(SIDEBAR[1], state='disabled')
            self.__file_menu_1.entryconfig(SIDEBAR[3], state='disabled')
            self.__file_menu_1.entryconfig(SIDEBAR[2], state='normal')
            self.__file_menu_1.entryconfig(SIDEBAR[9], state='disabled')
            for spinbox in self.__spinbox_elements:
                spinbox.config(state='disabled')
                spinbox.unbind('<KeyRelease>')
            for radiobutton in self.__radiobutton_elements:
                radiobutton.configure(state='disabled')
            for i in range(len(CLIENTS_OPTIONS)):
                self.__file_menu_clients.entryconfig(i, state='disabled')
            for i in range(2):
                self.__file_menu_sons.entryconfig(i, state='disabled')
                self.__file_menu_muts.entryconfig(i, state='disabled')
        else:
            self.__file_menu_1.entryconfig(SIDEBAR[1], state='normal')
            self.__file_menu_1.entryconfig(SIDEBAR[3], state='normal')
            self.__file_menu_1.entryconfig(SIDEBAR[2], state='disabled')
            self.__file_menu_1.entryconfig(SIDEBAR[9], state='normal')
            if self.__clear_on_next:
                for spinbox in self.__spinbox_elements:
                    spinbox.config(state='normal')
                    spinbox.bind('<KeyRelease>', self.on_spinbox_change)
                for radiobutton in self.__radiobutton_elements:
                    radiobutton.configure(state='normal')
                for client in range(len(CLIENTS_OPTIONS)):
                    self.__file_menu_clients.entryconfig(client, state='normal')
                for i in range(2):
                    self.__file_menu_sons.entryconfig(i, state='normal')
                    self.__file_menu_muts.entryconfig(i, state='normal')

    def __default_simulation(self) -> None:
        """
        Notifica a los observadores que se quieren ver soluciones
        predeterminadas.
        """

        self.sound.stop_sound()
        if self.__clear_on_next:
            self.__restart_simulation()
            self.__clear_on_next = False
        self.__toggle_simulation_state()
        df = self.CLIENT_LISTS[self.__clients_var.get()]
        coordinates = list(zip(df['XCOORD.'], df['YCOORD.']))
        self.__plotter.set_coordinates(coordinates)
        name = DEFAULT_NAME.format(
            CLIENTS_OPTIONS[self.__clients_var.get()]
        )
        parameters = (self.__clients_var.get(), name)
        self.notify_observers('default', parameters)

    def __start_simulation(self) -> None:
        """
        Notifica a los observadores que se inició la simulación.
        """

        self.sound.stop_sound()
        self.__is_paused = False
        if self.__clear_on_next:
            self.__restart_simulation()
            self.__is_paused = False
            self.__clear_on_next = False
        self.__toggle_simulation_state()
        df = self.CLIENT_LISTS[self.__clients_var.get()]
        if not self.__manual_stopped:
            coordinates = list(zip(df['XCOORD.'], df['YCOORD.']))
            self.__plotter.set_coordinates(coordinates)
        name = FILE_NAME.format(
            CLIENTS_OPTIONS[self.__clients_var.get()],
            self.__population_size_var.get(),
            self.__mutation_rate_var.get(),
            SELECTION_OPTIONS[self.__selection_var.get()],
            CROSSOVER_OPTIONS[self.__crossover_var.get()],
            FITNESS_OPTIONS[self.__fitness_var.get()],
            ELITISM_OPTIONS[self.__sons_var.get()+1],
            ELITISM_OPTIONS[self.__muts_var.get()+4]
        )
        parameters = (
            self.__clients_var.get(), int(self.__max_capacity_var.get()),
            float(self.__max_time_var.get()), int(self.__iterations_var.get()),
            int(self.__non_productive_var.get()),
            int(self.__population_size_var.get()),
            int(self.__no_child_production_var.get()),
            float(self.__mutation_rate_var.get()), self.__selection_var.get(),
            self.__crossover_var.get(), int(self.__max_distance_var.get()),
            self.__fitness_var.get(), int(self.__min_vehicles_var.get()),
            int(self.__max_vehicles_var.get()),
            name, self.__sons_var.get(), self.__muts_var.get()
        )
        self.notify_observers('set_parameters', parameters)
        self.notify_observers('start')

    def __stop_simulation(self) -> None:
        """
        Notifica a los observadores que la simulación debe ser
        detenida.
        """

        self.__is_paused = True
        self.__toggle_simulation_state()
        self.__file_menu_1.entryconfig(SIDEBAR[9], state='disabled')
        self.notify_observers('stop')
        self.__manual_stopped = True

    def __restart_simulation(self) -> None:
        """
        Notifica a los observadores que se reinició la simulación y
        borra el resultado anterior.
        """

        self.__is_paused = True
        self.__clear_on_next = True
        self.__toggle_simulation_state()
        if self.__manual_stopped:
            self.notify_observers('save')
            name = FILE_NAME.format(
                CLIENTS_OPTIONS[self.__clients_var.get()],
                self.__population_size_var.get(),
                self.__mutation_rate_var.get(),
                SELECTION_OPTIONS[self.__selection_var.get()],
                CROSSOVER_OPTIONS[self.__crossover_var.get()],
                FITNESS_OPTIONS[self.__fitness_var.get()],
                ELITISM_OPTIONS[self.__sons_var.get()+1],
                ELITISM_OPTIONS[self.__muts_var.get()+4]
            )
            self.__plotter.save_as_jpg(name)
        self.notify_observers('reset')
        self.__plotter.reset()
        self.__result_label['text'] = LABELS[10].format(0, 0)
        self.__file_menu_1.entryconfig(SIDEBAR[3], state='disabled')
        self.sound.stop_sound()
        self.__manual_stopped = False

    def __quit(self) -> None:
        """
        Termina el programa y avisa a los observadores que ya no
        continuen la simulación si es que tienen una.
        """

        self.sound.stop_sound()
        if not self.__is_paused:
            self.__stop_simulation()
            self.__restart_simulation()
        self.__window.quit()

    def update(self, event: str, data: Any=None) -> None:
        """
        Método que realiza acciones al recibir una notificación.

        Args:
            event (str): Banderas que identifican al evento:
                * "next_gen" Indica que se obtuvo una nueva generación
                    para y debe ser representado su resultado,
                * "stop" Indica que ha terminado la ejecución y ya no
                    habrá más resultados para la ejecución actual.
            data (Any): Un dato que es pasado al reallizarse la
                notificación:
                * "next_gen" Tiene el número de iteración y el mejor
                    individuo,
                * "stop" No tiene datos.
        """

        if event == 'next_gen':
            result = LABELS[10].format(data[0], data[1])
            if not self.__is_paused:
                if data[1] == 0:
                    self.__plotter.set_lines(list(data[2].routes))
                self.__result_label['text'] = result
        elif event == 'stop':
            self.__result_label['text'] = self.__result_label['text'] + LABELS[11]
            name = FILE_NAME.format(
                CLIENTS_OPTIONS[self.__clients_var.get()],
                self.__population_size_var.get(),
                self.__mutation_rate_var.get(),
                SELECTION_OPTIONS[self.__selection_var.get()],
                CROSSOVER_OPTIONS[self.__crossover_var.get()],
                FITNESS_OPTIONS[self.__fitness_var.get()],
                ELITISM_OPTIONS[self.__sons_var.get()+1],
                ELITISM_OPTIONS[self.__muts_var.get()+4]
            )
            self.__plotter.save_as_jpg(name)
            self.__is_paused = True
            self.__clear_on_next = True
            self.__toggle_simulation_state()
            self.__manual_stopped = False
        elif event == 'default':
            self.__plotter.set_lines(list(data.routes))
            self.__plotter.save_as_jpg(
                DEFAULT_NAME.format(CLIENTS_OPTIONS[self.__clients_var.get()])
            )
            self.__is_paused = True
            self.__clear_on_next = True
            self.__toggle_simulation_state()

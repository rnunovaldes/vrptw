#!/bin/bash

EXEC="src/main.py"
PY_PRIN="src/main.py"
PY_CONT1="src/mvc/controller/manager.py"
PY_CONT2="src/mvc/controller/observer.py"
PY_MODE1="src/mvc/model/model.py"
PY_MODE2="src/mvc/model/genetic_algorithm.py"
PY_MODE3="src/mvc/model/chromosome.py"
PY_VIEW1="src/mvc/view/window.py"
PY_VIEW2="src/mvc/view/plotter.py"
FILES_EXIST="YES"

if [ ! -f "$PY_PRIN" ]; then
    FILES_EXIST="NO"
else
    PY_FILES=("$PY_CONT1" "$PY_CONT2" "$PY_MODE1" "$PY_MODE2" "$PY_MODE3" "$PY_VIEW1")
    for FILE in "${PY_FILES[@]}"; do
        if [ ! -f "$FILE" ]; then
            FILES_EXIST="NO"
            exit 1
        fi
    done
fi

if [ "$FILES_EXIST" = "YES" ]; then
    echo "Vamos a ejecutar el programa"
    python3 "$EXEC"
else
    echo "Faltan archivos necesarios para ejecutar el programa."
fi

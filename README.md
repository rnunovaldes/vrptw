# Problema de Enrutamiento de Vehículos con Ventanas de Tiempo (VRPTW) - Algoritmo Genético

## Uso del los progrmas
Se tienen dos scripts llamados `program`, uno es `.sh` y el otro `.bat` para
que pueda ejecutarse el programa tanto en Unix como en Windows. Solo hay que
ejecutar el script correspondiente.

## Organización de directorios
```
/
├── src/
│   ├── mvc/...
│   └── main.py
├── res/...
├── program.sh
├── program.bat
└── README.md
```

  * En "src" está lo necesario para los programas 😎️.
  * En "res" hay recursos `csv` que son usados por los programas.

## Programa
<div style="display: flex; align-items: center;">
  <a href="https://www.python.org/">
  <img src="https://s3.dualstack.us-east-2.amazonaws.com/pythondotorg-assets/media/community/logos/python-logo-only.png" alt="The Python Logo" width="70px" />
</a>
  <p style="margin-left: 10px;">Este trabajo se programó con Python 3.11.3</p>
</div>
Para la ejecución, use alguno de los scripts proporcionados

###### Requisitos
* Pandas==2.1.1
* Pillow==10.0.1

## Autor
- Raúl N. Valdés
